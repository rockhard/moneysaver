//  AccountBookCategoryController.h


#import <UIKit/UIKit.h>
@class MoneySaverDatabase;
@class AccountBookController;

@interface AccountBookCategoryController : UIViewController 
<UITableViewDelegate, UITableViewDataSource> {
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_itemsList;
	UITableView				*m_itemsTableView;
	
	AccountBookController	*m_parent;
}

@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) NSMutableArray		*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView	*m_itemsTableView;
@property (retain, nonatomic) AccountBookController	*m_parent;

- (void) doReloadData;
@end

//  MoneySaverDatabase.m

#import "MoneySaverDatabase.h"
#define DB_FILE_NAME	@"moneysaver.db"

@implementation MoneySaverDatabase
@synthesize m_dbPath;


- (id) init {
	if(self = [super init]) {
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentDirectory = [paths objectAtIndex:0];
		
		self.m_dbPath = [documentDirectory stringByAppendingPathComponent:DB_FILE_NAME];
	}
	
	return self;
}

- (void) dealloc {
	[self.m_dbPath release];
	[super dealloc];
    NSLog(@"DEALLOC base");
}

- (bool) doCreateQuery:(NSString *)sql {
	char *errorMsg;
	int ret = sqlite3_exec(m_database, [sql UTF8String], NULL, NULL, &errorMsg);
	if(ret != SQLITE_OK) {
		sqlite3_close(m_database);
		NSLog(@"Error Query:%s", sqlite3_errmsg(m_database));
		return false;
	}
	return true;
}

- (bool) doOpenDatabase {
	
	int ret = sqlite3_open([self.m_dbPath UTF8String], &m_database);
	if(ret != SQLITE_OK) {
		sqlite3_close(m_database);
		NSAssert(0, @"Failed to open databases");
		return false;
	}
	return true;
}

- (bool) doCloseDatabase {
	if(m_database)
		sqlite3_close(m_database);
	return true;
}

#pragma mark -
#pragma mark Items Table

- (bool) doCreateItemsTable {
	NSString *createSQL = @"CREATE TABLE IF NOT EXISTS items \
	(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, type INTEGER)";

	if([self doCreateQuery:createSQL])
		return true;
	return false;
}

- (bool) doDeleteItemsTable:(NSInteger)items_id {
	NSString *deleteQuery = [[NSString alloc] initWithFormat:@"DELETE FROM items WHERE id = %d", items_id];
	
	if([self doCreateQuery:deleteQuery]) {
		NSLog(@"delect id=%d Success", items_id);
		[deleteQuery release];
		return true;
	}
	
	[deleteQuery release];
	return false;
}

- (bool) doInsertItemsTable:(NSMutableDictionary *)datas {

	NSString *name = [datas objectForKey:@"name"];
	int type = [[datas objectForKey:@"type"] intValue];
	
	NSString *insertSQL = [[NSString alloc] initWithFormat:@"INSERT INTO items \
						   VALUES(NULL, '%@', %d)", name, type];
	NSLog(@"%@", insertSQL);
	
	const char *sql = [insertSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		if(sqlite3_step(stmt) != SQLITE_DONE) {
			[insertSQL release];
			NSLog(@"insert step failed");
			return false;
		}
		sqlite3_finalize(stmt);
		NSLog(@"insert item table success");
		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[insertSQL release];
		return false;
	}
	
	[insertSQL release];
	return true;

}

- (NSMutableArray *) doSelectItemsTable {
	NSString *selectSQL = @"SELECT * from items";
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int tmpID = sqlite3_column_int(stmt, 0);
			const char *tmpName = (const char *)sqlite3_column_text(stmt, 1);
			int tmpType = sqlite3_column_int(stmt, 2);
			
			NSNumber *dataID = [[NSNumber alloc] initWithInt:tmpID];
			NSString *dataName = [[NSString alloc] initWithUTF8String:tmpName];
			NSNumber *dataType = [[NSNumber alloc] initWithInt:tmpType];
			
			NSLog(@"ID: %@ NAME: %@ TYPE: %@", dataID, dataName, dataType);
			
			[datas setObject:dataID forKey:@"id"];
			[datas setObject:dataName forKey:@"name"];
			[datas setObject:dataType forKey:@"type"];
			
			[dataID release];
			[dataName release];
			[dataType release];
			
			
			[dataArray addObject:datas];
			[datas release];
		}		
		sqlite3_finalize(stmt);		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"Select Success");
	return [dataArray autorelease];
}

#pragma mark -
#pragma mark bankbook Table

- (bool) doCreateBankbookTable {
	NSString *createSQL = @"CREATE TABLE IF NOT EXISTS bankbook \
	(id INTEGER PRIMARY KEY AUTOINCREMENT, items_id INTEGER, account TEXT, bank TEXT, datetime INTEGER, name TEXT)";
	
	if([self doCreateQuery:createSQL])
		return true;
	return false;
}

- (bool) doDeleteBankbookTable:(NSInteger)bankbook_id {
	NSString *deleteQuery = [[NSString alloc] initWithFormat:@"DELETE FROM bankbook WHERE id = %d", bankbook_id];
	
	if([self doCreateQuery:deleteQuery]) {
		NSLog(@"delect id=%d Success", bankbook_id);
		[deleteQuery release];
		return true;
	}
	
	[deleteQuery release];
	return false;
}

- (bool) doInsertBankbookTable:(NSMutableDictionary *)datas {
	
	NSString *dataAccount = [datas objectForKey:@"account"];
	NSString *dataBank = [datas objectForKey:@"bank"];
	NSString *dataName = [datas objectForKey:@"name"];
	int dataItemsId = [[datas objectForKey:@"items_id"] intValue];
	int dataDateTime = [[datas objectForKey:@"datetime"] intValue];
	

	
	NSString *insertSQL = [[NSString alloc] initWithFormat:@"INSERT INTO bankbook \
						   VALUES(NULL, %d, '%@', '%@', %d, '%@')",
						   dataItemsId, dataAccount, dataBank, dataDateTime, dataName];
	NSLog(@"%@", insertSQL);
	
	const char *sql = [insertSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		if(sqlite3_step(stmt) != SQLITE_DONE) {
			[insertSQL release];
			NSLog(@"insert step failed");
			return false;
		}
		sqlite3_finalize(stmt);
		NSLog(@"insert item table success");
		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[insertSQL release];
		return false;
	}	

	[insertSQL release];
	return true;
}

- (NSMutableArray *) doSelectBankbookTable:(NSInteger) items_id {
	
	NSString *selectSQL = nil;
	if(items_id != -1){
		selectSQL = [[NSString alloc] initWithFormat: @"SELECT * from bankbook where items_id = %d", items_id];
	}else{
		selectSQL = [[NSString alloc] initWithFormat:@"SELECT * from bankbook"];
	}
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int tmpID = sqlite3_column_int(stmt, 0);
			int tmpItemsId = sqlite3_column_int(stmt, 1);
			const char *tmpAccount = (const char *)sqlite3_column_text(stmt, 2);
			const char *tmpBank = (const char *)sqlite3_column_text(stmt, 3);
			int tmpDatetime = sqlite3_column_int(stmt, 4);
			const char *tmpName = (const char *)sqlite3_column_text(stmt, 5);
			
			NSNumber *dataID = [[NSNumber alloc] initWithInt:tmpID];
			NSNumber *dataItemsId = [[NSNumber alloc] initWithInt:tmpItemsId];
			NSString *dataAccount = [[NSString alloc] initWithUTF8String:tmpAccount];
			NSString *dataBank = [[NSString alloc] initWithUTF8String:tmpBank];
			NSNumber *dataDateTime = [[NSNumber alloc] initWithInt:tmpDatetime];
			NSString *dataName = [[NSString alloc] initWithUTF8String:tmpName];
			
			NSLog(@"ID: %@ ITEMID: %@ ACCount: %@", dataID, dataItemsId, dataAccount);
			
			[datas setObject:dataID forKey:@"id"];
			[datas setObject:dataItemsId forKey:@"items_id"];
			[datas setObject:dataAccount forKey:@"account"];
			[datas setObject:dataBank forKey:@"bank"];
			[datas setObject:dataDateTime forKey:@"datetime"];
			[datas setObject:dataName forKey:@"name"];
			
			[dataID release];
			[dataItemsId release];
			[dataAccount release];
			[dataBank release];
			[dataDateTime release];
			[dataName release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		
		sqlite3_finalize(stmt);		
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"Select Success");
	[selectSQL release];
	return [dataArray autorelease];
	
}

- (NSMutableArray *) doSelectBankbookTableId:(NSInteger) bankbook_id {
	
	NSString *selectSQL = [[NSString alloc] initWithFormat: @"SELECT * from bankbook where id = %d", bankbook_id];
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int tmpID = sqlite3_column_int(stmt, 0);
			int tmpItemsId = sqlite3_column_int(stmt, 1);
			const char *tmpAccount = (const char *)sqlite3_column_text(stmt, 2);
			const char *tmpBank = (const char *)sqlite3_column_text(stmt, 3);
			int tmpDatetime = sqlite3_column_int(stmt, 4);
			const char *tmpName = (const char *)sqlite3_column_text(stmt, 5);
			
			NSNumber *dataID = [[NSNumber alloc] initWithInt:tmpID];
			NSNumber *dataItemsId = [[NSNumber alloc] initWithInt:tmpItemsId];
			NSString *dataAccount = [[NSString alloc] initWithUTF8String:tmpAccount];
			NSString *dataBank = [[NSString alloc] initWithUTF8String:tmpBank];
			NSNumber *dataDateTime = [[NSNumber alloc] initWithInt:tmpDatetime];
			NSString *dataName = [[NSString alloc] initWithUTF8String:tmpName];
			
			NSLog(@"ID: %@ ITEMID: %@ ACCount: %@", dataID, dataItemsId, dataAccount);
			
			[datas setObject:dataID forKey:@"id"];
			[datas setObject:dataItemsId forKey:@"items_id"];
			[datas setObject:dataAccount forKey:@"account"];
			[datas setObject:dataBank forKey:@"bank"];
			[datas setObject:dataDateTime forKey:@"datetime"];
			[datas setObject:dataName forKey:@"name"];
			
			[dataID release];
			[dataItemsId release];
			[dataAccount release];
			[dataBank release];
			[dataDateTime release];
			[dataName release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		
		sqlite3_finalize(stmt);		
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[dataArray release];
		[selectSQL release];
		return dataArray;
	}
	
	NSLog(@"Select Success");
	[selectSQL release];
	return [dataArray autorelease];
	
}


#pragma mark -
#pragma mark Accountbook Table

- (bool) doCreateAccountbookTable {
	NSString *createSQL = @"CREATE TABLE IF NOT EXISTS accountbook \
	(id INTEGER PRIMARY KEY AUTOINCREMENT, items_id INTEGER, category_pid INTEGER, category_id INTEGER, bankbook_id INTEGER, \
	income REAL, expense REAL, name TEXT, description TEXT, datetime DOUBLE)";
	
	if([self doCreateQuery:createSQL])
		return true;
	return false;
	
}

- (bool) doDeleteAccountbookTable:(NSInteger)account_id {
	NSString *deleteQuery = [[NSString alloc] initWithFormat:@"DELETE FROM accountbook WHERE id = %d", account_id];
	
	if([self doCreateQuery:deleteQuery]) {
		NSLog(@"delect id=%d Success", account_id);
		[deleteQuery release];
		return true;
	}
	
	[deleteQuery release];
	return false;
	
}

- (bool) doUpdateAccountbookTable:(NSMutableDictionary *)datas number:(NSInteger)account_id{
	int dataItemsId = [[datas objectForKey:@"items_id"] intValue];
	int dataCategoryPid = [[datas objectForKey:@"category_pid"] intValue];
	int dataCategoryId = [[datas objectForKey:@"category_id"] intValue];
	int dataBankbookId = [[datas objectForKey:@"bankbook_id"] intValue];
	double dataIncome = [[datas objectForKey:@"income"] doubleValue];
	double dataExpense = [[datas objectForKey:@"expense"] doubleValue];
	NSString *dataName = [datas objectForKey:@"name"];
	NSString *dataDescription = [datas objectForKey:@"description"];
	double dataDateTime = [[datas objectForKey:@"datetime"] doubleValue];
	
	
	
	NSString *insertSQL = [[NSString alloc] initWithFormat:@"UPDATE accountbook \
						   SET items_id = %d, category_pid = %d, category_id = %d, bankbook_id = %d, income = %f, expense = %f, name = '%@', description = '%@', datetime = %f \
						   WHERE id = %d",
						   dataItemsId, dataCategoryPid, dataCategoryId, dataBankbookId, 
						   dataIncome, dataExpense, dataName, dataDescription, dataDateTime, account_id];
	NSLog(@"%@", insertSQL);
	const char *sql = [insertSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		if(sqlite3_step(stmt) != SQLITE_DONE) {
			[insertSQL release];
			NSLog(@"insert step failed %s", sqlite3_errmsg(m_database));
			return false;
		}
		sqlite3_finalize(stmt);
		NSLog(@"insert item table success");
		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[insertSQL release];
		return false;
	}	
	
	[insertSQL release];
	return true;	
	
}

- (bool) doInsertAccountbookTable:(NSMutableDictionary *)datas {
	int dataItemsId = [[datas objectForKey:@"items_id"] intValue];
	int dataCategoryPid = [[datas objectForKey:@"category_pid"] intValue];
	int dataCategoryId = [[datas objectForKey:@"category_id"] intValue];
	int dataBankbookId = [[datas objectForKey:@"bankbook_id"] intValue];
	double dataIncome = [[datas objectForKey:@"income"] doubleValue];
	double dataExpense = [[datas objectForKey:@"expense"] doubleValue];
	NSString *dataName = [datas objectForKey:@"name"];
	NSString *dataDescription = [datas objectForKey:@"description"];
	double dataDateTime = [[datas objectForKey:@"datetime"] doubleValue];
	

	
	NSString *insertSQL = [[NSString alloc] initWithFormat:@"INSERT INTO accountbook \
						   VALUES(NULL, %d, %d, %d, %d, %f, %f, '%@', '%@', %f)",
						   dataItemsId, dataCategoryPid, dataCategoryId, dataBankbookId, 
						   dataIncome, dataExpense, dataName, dataDescription, dataDateTime];
	NSLog(@"%@", insertSQL);
	const char *sql = [insertSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		if(sqlite3_step(stmt) != SQLITE_DONE) {
			[insertSQL release];
			NSLog(@"insert step failed %s", sqlite3_errmsg(m_database));
			return false;
		}
		sqlite3_finalize(stmt);
		NSLog(@"insert item table success");
		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[insertSQL release];
		return false;
	}	
	
	[insertSQL release];
	return true;	
	
}

- (NSMutableDictionary *) doSelectAccountbookTableSum:(NSInteger)items_id {

	NSString *selectSQL = [[NSString alloc] initWithFormat:@"SELECT SUM(income), SUM(expense) from accountbook where items_id = %d", items_id];
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			double incomeValue = sqlite3_column_double(stmt, 0);
			double expenseValue = sqlite3_column_double(stmt, 1);
			
			NSNumber *accountbookIncome = [[NSNumber alloc] initWithDouble:incomeValue];
			NSNumber *accountbookExpense = [[NSNumber alloc] initWithDouble:expenseValue];
			
			[datas setObject:accountbookIncome forKey:@"income"];
			[datas setObject:accountbookExpense forKey:@"expense"];
			
			[accountbookIncome release];
			[accountbookExpense release];
			
			NSLog(@"income: %.1f expense: %.1f", incomeValue, expenseValue);
		}		
		sqlite3_finalize(stmt);		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[datas release];
		return datas;
	}
	
	NSLog(@"Select Success");
	[selectSQL release];
	return [datas autorelease];
}

- (NSMutableArray *) doSelectAccountbookTable:(NSInteger)accountbook_id {
	NSString *selectSQL = [[NSString alloc] initWithFormat:@"select * from accountbook where id = %d", accountbook_id];
	NSLog(@"%@", selectSQL);
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int accountbookIdValue = sqlite3_column_int(stmt, 0);
			int accountbookItemIdValue = sqlite3_column_int(stmt, 1);
			int accountbookCategoryPidValue = sqlite3_column_int(stmt, 2);
			int accountbookCategoryIdValue = sqlite3_column_int(stmt, 3);
			int accountbookBankIdValue = sqlite3_column_int(stmt, 4);
			double accountbookIncomeValue= sqlite3_column_double(stmt, 5);
			double accountbookExpenseValue = sqlite3_column_double(stmt, 6);
			const char *accountbookNameValue = (const char *)sqlite3_column_text(stmt, 7);
			const char *accountbookDescValue = (const char *)sqlite3_column_text(stmt, 8);
			double accountbookDateTimeValue = sqlite3_column_double(stmt, 9);
			
			NSNumber *accountbookID = [[NSNumber alloc] initWithInt:accountbookIdValue];
			NSNumber *accountbookItemId = [[NSNumber alloc] initWithInt:accountbookItemIdValue];
			NSNumber *accountbookCategoryPid = [[NSNumber alloc] initWithInt:accountbookCategoryPidValue];
			NSNumber *accountbookCategoryId = [[NSNumber alloc] initWithInt:accountbookCategoryIdValue];
			NSNumber *accountbookBankId = [[NSNumber alloc] initWithInt:accountbookBankIdValue];
			NSNumber *accountbookIncome = [[NSNumber alloc] initWithDouble:accountbookIncomeValue];
			NSNumber *accountbookExpense = [[NSNumber alloc] initWithDouble:accountbookExpenseValue];
			NSString *accountbookName = [[NSString alloc] initWithUTF8String:accountbookNameValue];
			NSString *accountbookDesc = [[NSString alloc] initWithUTF8String:accountbookDescValue];
			NSNumber *accountbookDateTime = [[NSNumber alloc] initWithDouble:accountbookDateTimeValue];
			
			NSLog(@"id: %@ itmeid:%@ name %@ date %@ bankbook %@", accountbookID, accountbookItemId, accountbookName, accountbookDateTime, accountbookBankId);
			
			[datas setObject:accountbookID forKey:@"id"];
			[datas setObject:accountbookItemId forKey:@"items_id"];
			[datas setObject:accountbookCategoryPid forKey:@"category_pid"];
			[datas setObject:accountbookCategoryId forKey:@"category_id"];
			[datas setObject:accountbookBankId forKey:@"bankbook_id"];
			[datas setObject:accountbookIncome forKey:@"income"];
			[datas setObject:accountbookExpense forKey:@"expense"];
			[datas setObject:accountbookName forKey:@"name"];
			[datas setObject:accountbookDesc forKey:@"description"];
			[datas setObject:accountbookDateTime forKey:@"datetime"];
			
			[accountbookID release];
			[accountbookItemId release];
			[accountbookCategoryPid release];
			[accountbookCategoryId release];
			[accountbookBankId release];
			[accountbookIncome release];
			[accountbookExpense release];
			[accountbookName release];
			[accountbookDesc release];
			[accountbookDateTime release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"select success");
	[selectSQL release];
	return [dataArray autorelease];
	
}

- (NSMutableArray *) doSelectAccountbookTable:(NSInteger)items_id number:(NSInteger)limit {
	NSString *selectSQL = [[NSString alloc] initWithFormat:
				@"select accountbook.id, accountbook.income, accountbook.expense, accountbook.name, category.image, category.name, category.type, strftime('%%Y-%%m-%%d', accountbook.datetime, 'unixepoch', 'localtime') \
				from accountbook, category where accountbook.category_id = category.id and items_id = %d \
				order by datetime desc limit %d", items_id, limit];
	
	NSLog(@"%@", selectSQL);
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int accountbookIdValue = sqlite3_column_int(stmt, 0);
			double accountbookIncomeValue = sqlite3_column_double(stmt, 1);
			double accountbookExpenseValue = sqlite3_column_double(stmt, 2);
			const char *accountbookNameValue = (const char *)sqlite3_column_text(stmt, 3);
			const char *categoryImageValue = (const char *)sqlite3_column_text(stmt, 4);
			const char *categoryNameValue = (const char *)sqlite3_column_text(stmt, 5);
			int categoryTypeValue = sqlite3_column_int(stmt, 6);
			const char *dateValue = (const char *)sqlite3_column_text(stmt, 7);
			
			NSNumber *accountID = [[NSNumber alloc] initWithInt:accountbookIdValue];
			NSNumber *accountIncome = [[NSNumber alloc] initWithDouble:accountbookIncomeValue];
			NSNumber *accountExpense = [[NSNumber alloc] initWithDouble:accountbookExpenseValue];
			NSString *accountName = [[NSString alloc] initWithUTF8String:accountbookNameValue];
			NSString *categoryImage = [[NSString alloc] initWithUTF8String:categoryImageValue];
			NSString *categoryName = [[NSString alloc] initWithUTF8String:categoryNameValue];
			NSNumber *categoryType = [[NSNumber alloc] initWithInt:categoryTypeValue];
			NSString *accountDate = [[NSString alloc] initWithUTF8String:dateValue];
			
			NSLog(@"name: %@ image:%@ name %@ date %@", accountName, categoryImage, categoryName, accountDate);
			
			[datas setObject:accountID forKey:@"id"];
			[datas setObject:accountIncome forKey:@"income"];
			[datas setObject:accountExpense forKey:@"expense"];
			[datas setObject:accountName forKey:@"name"];
			[datas setObject:categoryImage forKey:@"image"];
			[datas setObject:categoryName forKey:@"image_name"];
			[datas setObject:categoryType forKey:@"type"];
			[datas setObject:accountDate forKey:@"date"];
			
			[accountID release];
			[accountIncome release];
			[accountExpense release];
			[accountName release];
			[categoryName release];
			[categoryImage release];
			[categoryType release];
			[accountDate release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"select success");
	[selectSQL release];
	return [dataArray autorelease];
}

- (NSMutableArray *) doSelectAccountbookTableDayItems:(NSInteger)items_id number:(NSString *)date {
	NSString *selectSQL = [[NSString alloc] initWithFormat:
						   @"select accountbook.id, accountbook.income, accountbook.expense, accountbook.name, category.image, category.name, category.type, strftime('%%Y-%%m-%%d', accountbook.datetime, 'unixepoch') \
						   from accountbook, category where accountbook.category_id = category.id and items_id = %d and date(accountbook.datetime, 'unixepoch') = '%@' \
						   order by datetime desc", items_id, date];
	
	NSLog(@"%@", selectSQL);
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int accountbookIdValue = sqlite3_column_int(stmt, 0);
			double accountbookIncomeValue = sqlite3_column_double(stmt, 1);
			double accountbookExpenseValue = sqlite3_column_double(stmt, 2);
			const char *accountbookNameValue = (const char *)sqlite3_column_text(stmt, 3);
			const char *categoryImageValue = (const char *)sqlite3_column_text(stmt, 4);
			const char *categoryNameValue = (const char *)sqlite3_column_text(stmt, 5);
			int categoryTypeValue = sqlite3_column_int(stmt, 6);
			const char *accountDateValue = (const char *)sqlite3_column_text(stmt, 7);
			
			NSNumber *accountID = [[NSNumber alloc] initWithInt:accountbookIdValue];
			NSNumber *accountIncome = [[NSNumber alloc] initWithDouble:accountbookIncomeValue];
			NSNumber *accountExpense = [[NSNumber alloc] initWithDouble:accountbookExpenseValue];
			NSString *accountName = [[NSString alloc] initWithUTF8String:accountbookNameValue];
			NSString *categoryImage = [[NSString alloc] initWithUTF8String:categoryImageValue];
			NSString *categoryName = [[NSString alloc] initWithUTF8String:categoryNameValue];
			NSNumber *categoryType = [[NSNumber alloc] initWithInt:categoryTypeValue];
			NSString *accountDate = [[NSString alloc] initWithUTF8String:accountDateValue];
			
			NSLog(@"name: %@ image:%@ name %@", accountName, categoryImage, categoryName);
			
			[datas setObject:accountID forKey:@"id"];
			[datas setObject:accountIncome forKey:@"income"];
			[datas setObject:accountExpense forKey:@"expense"];
			[datas setObject:accountName forKey:@"name"];
			[datas setObject:categoryImage forKey:@"image"];
			[datas setObject:categoryName forKey:@"image_name"];
			[datas setObject:categoryType forKey:@"type"];
			[datas setObject:accountDate forKey:@"date"];
			
			[accountID release];
			[accountIncome release];
			[accountExpense release];
			[accountName release];
			[categoryName release];
			[categoryImage release];
			[categoryType release];
			[accountDate release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"select success");
	[selectSQL release];
	return [dataArray autorelease];
}

- (NSMutableArray *) doSelectAccountbookTableDateSum:(NSInteger)items_id limit:(NSInteger)count {
	NSString *selectSQL = [[NSString alloc] initWithFormat:@"SELECT date(datetime, 'unixepoch'), sum(income), sum(expense), count(id) from accountbook \
						   WHERE items_id = %d group by date(datetime, 'unixepoch') order by date(datetime, 'unixepoch') desc limit %d", items_id, count];
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);

	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			const char *tmpDate = (const char *)sqlite3_column_text(stmt, 0);
			double tmpIcomeSum = sqlite3_column_double(stmt, 1);
			double tmpExpenseSum = sqlite3_column_double(stmt, 2);
			int tmpCount = sqlite3_column_int(stmt, 3);
			
			NSString *dataDateString = [[NSString alloc] initWithUTF8String:tmpDate];
			NSNumber *dataIncomeSum = [[NSNumber alloc] initWithDouble:tmpIcomeSum];
			NSNumber *dataExpenseSum = [[NSNumber alloc] initWithDouble:tmpExpenseSum];
			NSNumber *dataCount = [[NSNumber alloc] initWithInt:tmpCount];
			
			NSLog(@"datestring:%@ dataIncomesum:%f dataexpenseSum:%f dataCount:%d", dataDateString, tmpIcomeSum, tmpExpenseSum, tmpCount);
			
			[datas setObject:dataDateString forKey:@"date"];
			[datas setObject:dataIncomeSum forKey:@"income_sum"];
			[datas setObject:dataExpenseSum forKey:@"expense_sum"];
			[datas setObject:dataCount forKey:@"count"];
			
			[dataDateString release];
			[dataIncomeSum release];
			[dataExpenseSum release];
			[dataCount release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);	
	} else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"select Success");
	[selectSQL release];
	return [dataArray autorelease];

}

- (NSMutableArray *) doSelectAccountbookTableWeekSum:(NSInteger)items_id limit:(NSInteger)count {
	NSString *selectSQL = [[NSString alloc] initWithFormat:@"SELECT strftime('~%%Y-%%m-%%d',datetime, 'unixepoch','weekday 0'), sum(income), sum(expense), count(id) from accountbook \
						   WHERE items_id = %d group by strftime('%%W', datetime, 'unixepoch') order by strftime('%%W', datetime, 'unixepoch') desc limit %d", items_id, count];
	
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			const char *tmpDate = (const char *)sqlite3_column_text(stmt, 0);
			double tmpIcomeSum = sqlite3_column_double(stmt, 1);
			double tmpExpenseSum = sqlite3_column_double(stmt, 2);
			int tmpCount = sqlite3_column_int(stmt, 3);
			
			NSString *dataDateString = [[NSString alloc] initWithUTF8String:tmpDate];
			NSNumber *dataIncomeSum = [[NSNumber alloc] initWithDouble:tmpIcomeSum];
			NSNumber *dataExpenseSum = [[NSNumber alloc] initWithDouble:tmpExpenseSum];
			NSNumber *dataCount = [[NSNumber alloc] initWithInt:tmpCount];
			
			NSLog(@"datestring:%@ dataIncomesum:%f dataexpenseSum:%f dataCount:%d", dataDateString, tmpIcomeSum, tmpExpenseSum, tmpCount);
			
			[datas setObject:dataDateString forKey:@"date"];
			[datas setObject:dataIncomeSum forKey:@"income_sum"];
			[datas setObject:dataExpenseSum forKey:@"expense_sum"];
			[datas setObject:dataCount forKey:@"count"];
			
			[dataDateString release];
			[dataIncomeSum release];
			[dataExpenseSum release];
			[dataCount release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);	
	} else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"select Success");
	[selectSQL release];
	return [dataArray autorelease];
	
}

- (NSMutableArray *) doSelectAccountbookTableMonthSum:(NSInteger)items_id limit:(NSInteger)count {
	NSString *selectSQL = [[NSString alloc] initWithFormat:@"SELECT strftime('%%Y-%%m', datetime, 'unixepoch'), sum(income), sum(expense), count(id) from accountbook \
						   WHERE items_id = %d group by strftime('%%Y-%%m', datetime, 'unixepoch') order by strftime('%%Y-%%m', datetime, 'unixepoch') desc limit %d", items_id, count];
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			const char *tmpDate = (const char *)sqlite3_column_text(stmt, 0);
			double tmpIcomeSum = sqlite3_column_double(stmt, 1);
			double tmpExpenseSum = sqlite3_column_double(stmt, 2);
			int tmpCount = sqlite3_column_int(stmt, 3);
			
			NSString *dataDateString = [[NSString alloc] initWithUTF8String:tmpDate];
			NSNumber *dataIncomeSum = [[NSNumber alloc] initWithDouble:tmpIcomeSum];
			NSNumber *dataExpenseSum = [[NSNumber alloc] initWithDouble:tmpExpenseSum];
			NSNumber *dataCount = [[NSNumber alloc] initWithInt:tmpCount];
			
			NSLog(@"datestring:%@ dataIncomesum:%f dataexpenseSum:%f dataCount:%d", dataDateString, tmpIcomeSum, tmpExpenseSum, tmpCount);
			
			[datas setObject:dataDateString forKey:@"date"];
			[datas setObject:dataIncomeSum forKey:@"income_sum"];
			[datas setObject:dataExpenseSum forKey:@"expense_sum"];
			[datas setObject:dataCount forKey:@"count"];
			
			[dataDateString release];
			[dataIncomeSum release];
			[dataExpenseSum release];
			[dataCount release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);	
	} else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"select Success");
	[selectSQL release];
	return [dataArray autorelease];
	
}

- (NSMutableArray *) doSelectAccountbookTableAllStatistic:(NSInteger)items_id type:(NSInteger)type_id {
	NSString *selectSQL = [[NSString alloc] initWithFormat:@"select category.image, category.name, sum(accountbook.income), sum(accountbook.expense) from accountbook, category \
						   where accountbook.category_pid = category.id and items_id = %d and category.type = %d group by accountbook.category_pid", items_id, type_id];
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			const char *tmpImage = (const char *)sqlite3_column_text(stmt, 0);
			const char *tmpName = (const char *)sqlite3_column_text(stmt, 1);
			double tmpIncome = sqlite3_column_double(stmt, 2);
			double tmpExpense = sqlite3_column_double(stmt, 3);
			
			NSString *accountbookImage = [[NSString alloc] initWithUTF8String:tmpImage];
			NSString *accountbookName = [[NSString alloc] initWithUTF8String:tmpName];
			NSNumber *accountbookIncome = [[NSNumber alloc] initWithDouble:tmpIncome];
			NSNumber *accountbookExpense = [[NSNumber alloc] initWithDouble:tmpExpense];
			
			NSLog(@"image: %@ name: %@ income: %@ expense: %@", accountbookImage, accountbookName, accountbookIncome, accountbookExpense);
			
			[datas setObject:accountbookImage forKey:@"image"];
			[datas setObject:accountbookName forKey:@"name"];
			[datas setObject:accountbookIncome forKey:@"income"];
			[datas setObject:accountbookExpense forKey:@"expense"];
			
			[accountbookName release];
			[accountbookImage release];
			[accountbookExpense release];
			[accountbookIncome release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		sqlite3_finalize(stmt);
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"Select Success");
	[selectSQL release];
	return [dataArray autorelease];
	
}

#pragma mark -
#pragma mark Category Table

- (bool) doCreateCategoryTable {
	NSString *createSQL = @"CREATE TABLE IF NOT EXISTS category \
	(id INTEGER PRIMARY KEY AUTOINCREMENT, pid INTEGER, name TEXT, image TEXT, click INTEGER, type INTEGER)";
	
	if([self doCreateQuery:createSQL])
		return true;
	return false;	
}

- (bool) doDeleteAllCategoryTable {
	NSString *deleteQuery = @"DELETE FROM category";
	
	if([self doCreateQuery:deleteQuery]) {
		NSLog(@"delete category id=%d Success");
		return true;
	}
	
	return false;		
}

- (bool) doDeleteCategoryTable:(NSInteger)category_id {
	NSString *deleteQuery = [[NSString alloc] initWithFormat:@"DELETE FROM category WHERE id = %d", category_id];
	
	if([self doCreateQuery:deleteQuery]) {
		NSLog(@"delete category id=%d Success", category_id);
		[deleteQuery release];
		return true;
	}
	
	[deleteQuery release];
	return false;	
}

- (bool) doInsertCategoryTable:(NSMutableDictionary *)datas {
	
	int pid = [[datas objectForKey:@"pid"] intValue];
	NSString *name = [datas objectForKey:@"name"];
	NSString *image = [datas objectForKey:@"image"];
	int click = [[datas objectForKey:@"click"] intValue];
	int type = [[datas objectForKey:@"type"] intValue];
	

	
	NSString *insertSQL = [[NSString alloc] initWithFormat:@"INSERT INTO category \
						   VALUES(NULL, %d, '%@', '%@', %d, %d)", pid, name, image, click, type];
	NSLog(@"%@", insertSQL);
	
	const char *sql = [insertSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		if(sqlite3_step(stmt) != SQLITE_DONE) {
			[insertSQL release];
			NSLog(@"insert step failed");
			return false;
		}
		sqlite3_finalize(stmt);
		NSLog(@"insert item table success");
		
	}else {
		NSLog(@"insert Failed %s", sqlite3_errmsg(m_database));
		[insertSQL release];
		return false;
	}
	
	[insertSQL release];
	return true;	
}

- (NSMutableArray *) doSelectCategoryTableId:(NSInteger)category_id {
	NSString *selectSQL = [[NSString alloc] initWithFormat:@"SELECT * FROM category WHERE id = %d", category_id];
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int idValue				= sqlite3_column_int(stmt, 0);
			int pidValue			= sqlite3_column_int(stmt, 1);
			const char *nameValue	= (const char *)sqlite3_column_text(stmt, 2);
			const char *imageValue	= (const char *)sqlite3_column_text(stmt, 3);
			int clickValue			= sqlite3_column_int(stmt, 4);
			int typeValue			= sqlite3_column_int(stmt, 5);
			
			NSNumber *dataId		= [[NSNumber alloc] initWithInt:idValue];
			NSNumber *dataPid		= [[NSNumber alloc] initWithInt:pidValue];
			NSString *dataName		= [[NSString alloc] initWithUTF8String:nameValue];
			NSString *dataImage		= [[NSString alloc] initWithUTF8String:imageValue];
			NSNumber *dataClick		= [[NSNumber alloc] initWithInt:clickValue];
			NSNumber *dataType		= [[NSNumber alloc] initWithInt:typeValue];
			
			NSLog(@"Category id:%d pid:%d name:%@", dataId, dataPid, dataName);
			
			[datas setObject:dataId		forKey:@"id"];
			[datas setObject:dataPid	forKey:@"pid"];
			[datas setObject:dataImage	forKey:@"image"];
			[datas setObject:dataName	forKey:@"name"];
			[datas setObject:dataClick	forKey:@"click"];
			[datas setObject:dataType	forKey:@"type"];
			
			[dataId release];
			[dataPid release];
			[dataImage release];
			[dataName release];
			[dataClick release];
			[dataType release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		
		sqlite3_finalize(stmt);		
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[selectSQL release];
		[dataArray release];
		return dataArray;
	}
	
	[selectSQL release];
	NSLog(@"Select Success");
	return [dataArray autorelease];	
}


- (NSMutableArray *) doSelectCategoryTable:(NSInteger)count {
	NSString *selectSQL = nil;
	if(count == 1){
		selectSQL = @"SELECT * from category order by click limit 1";
	}else{
		selectSQL = @"SELECT * from category";
	}
	NSLog(@"%@", selectSQL);
	
	const char *sql = [selectSQL UTF8String];
	sqlite3_stmt *stmt = NULL;
	
	int ret = -1;
	NSMutableArray *dataArray = [[NSMutableArray alloc] init];
	ret = sqlite3_prepare_v2(m_database, sql, -1, &stmt, nil);
	if(ret == SQLITE_OK) {
		
		while (sqlite3_step(stmt) == SQLITE_ROW) {
			NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
			
			int idValue				= sqlite3_column_int(stmt, 0);
			int pidValue			= sqlite3_column_int(stmt, 1);
			const char *nameValue	= (const char *)sqlite3_column_text(stmt, 2);
			const char *imageValue	= (const char *)sqlite3_column_text(stmt, 3);
			int clickValue			= sqlite3_column_int(stmt, 4);
			int typeValue			= sqlite3_column_int(stmt, 5);
			
			NSNumber *dataId		= [[NSNumber alloc] initWithInt:idValue];
			NSNumber *dataPid		= [[NSNumber alloc] initWithInt:pidValue];
			NSString *dataName		= [[NSString alloc] initWithUTF8String:nameValue];
			NSString *dataImage		= [[NSString alloc] initWithUTF8String:imageValue];
			NSNumber *dataClick		= [[NSNumber alloc] initWithInt:clickValue];
			NSNumber *dataType		= [[NSNumber alloc] initWithInt:typeValue];
			
			NSLog(@"Category id:%@ pid:%@ name:%@", dataId, dataPid, dataName);
			
			[datas setObject:dataId		forKey:@"id"];
			[datas setObject:dataPid	forKey:@"pid"];
			[datas setObject:dataImage	forKey:@"image"];
			[datas setObject:dataName	forKey:@"name"];
			[datas setObject:dataClick	forKey:@"click"];
			[datas setObject:dataType	forKey:@"type"];
			
			[dataId release];
			[dataPid release];
			[dataImage release];
			[dataName release];
			[dataClick release];
			[dataType release];
			
			[dataArray addObject:datas];
			[datas release];
		}
		
		sqlite3_finalize(stmt);		
	}else {
		NSLog(@"select Failed %s", sqlite3_errmsg(m_database));
		[dataArray release];
		return dataArray;
	}
	
	NSLog(@"Select Success");
	return [dataArray autorelease];	
}

#pragma mark -
#pragma mark Image Table

- (bool) doCreateImageTable {
	NSString *createSQL = @"CREATE TABLE IF NOT EXISTS stockbook \
	(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)";
	
	if([self doCreateQuery:createSQL])
		return true;
	return false;
}
@end

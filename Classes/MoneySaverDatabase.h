//  MoneySaverDatabase.h

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface MoneySaverDatabase : NSObject {	
	sqlite3 *m_database;
	NSString *m_dbPath;
}

@property (retain, nonatomic) NSString *m_dbPath;

- (bool) doOpenDatabase;
- (bool) doCloseDatabase;
- (bool) doCreateQuery:(NSString *)sql;

- (bool) doCreateItemsTable;
- (bool) doDeleteItemsTable:(NSInteger)items_id;
- (bool) doInsertItemsTable:(NSMutableDictionary *)datas;
- (NSMutableArray *) doSelectItemsTable;

- (bool) doCreateBankbookTable;
- (bool) doDeleteBankbookTable:(NSInteger)bankbook_id;
- (bool) doInsertBankbookTable:(NSMutableDictionary *)datas;
- (NSMutableArray *) doSelectBankbookTableId:(NSInteger)bankbook_id;
- (NSMutableArray *) doSelectBankbookTable:(NSInteger)items_id;

- (bool) doCreateCategoryTable;
- (bool) doDeleteAllCategoryTable;
- (bool) doDeleteCategoryTable:(NSInteger)category_id;
- (bool) doInsertCategoryTable:(NSMutableDictionary *)datas;
- (NSMutableArray *) doSelectCategoryTableId:(NSInteger)category_id;
- (NSMutableArray *) doSelectCategoryTable:(NSInteger)count;

- (bool) doCreateAccountbookTable;
- (bool) doDeleteAccountbookTable:(NSInteger)account_id;
- (bool) doUpdateAccountbookTable:(NSMutableDictionary *)datas number:(NSInteger)account_id;
- (bool) doInsertAccountbookTable:(NSMutableDictionary *)datas;
- (NSMutableDictionary *) doSelectAccountbookTableSum:(NSInteger)items_id;
- (NSMutableArray *) doSelectAccountbookTable:(NSInteger)accountbook_id;
- (NSMutableArray *) doSelectAccountbookTable:(NSInteger)items_id number:(NSInteger)limit;
- (NSMutableArray *) doSelectAccountbookTableDayItems:(NSInteger)items_id number:(NSString *)date;
- (NSMutableArray *) doSelectAccountbookTableDateSum:(NSInteger)items_id limit:(NSInteger)count;
- (NSMutableArray *) doSelectAccountbookTableWeekSum:(NSInteger)items_id limit:(NSInteger)count;
- (NSMutableArray *) doSelectAccountbookTableMonthSum:(NSInteger)items_id limit:(NSInteger)count;

- (NSMutableArray *) doSelectAccountbookTableAllStatistic:(NSInteger)items_id type:(NSInteger)type_id;
//- (NSMutableArray *) doSelectAccountbookTableMonthStatistic:(NSInteger)items_id type:(NSInteger)type_id date:(NSString *)date_string;

@end

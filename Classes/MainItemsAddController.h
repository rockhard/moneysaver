//  MainItemsAddController.h


#import <UIKit/UIKit.h>

@class MoneySaverDatabase;
@class MainItemsController;

@interface MainItemsAddController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	MoneySaverDatabase	*m_db;
	UITextField			*m_nameTextField;
	UISegmentedControl	*m_typeSegmentedControl;
	MainItemsController *m_parent;
}

@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) UITextField			*m_nameTextField;
@property (retain, nonatomic) UISegmentedControl	*m_typeSegmentedControl;
@property (retain, nonatomic) MainItemsController	*m_parent;

- (IBAction) doSaveButtonClick:(id)sender;
- (IBAction) doKeyboardDoneButtonClick:(id)sender;

@end

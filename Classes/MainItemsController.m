//  MainItemsController.m


#import "MainItemsController.h"
#import "MoneySaverDatabase.h"
#import "MainItemsAddController.h"
#import "AccountBookController.h"
#import "BankBookController.h"

#define ITEM_TYPE_ACCOUNT 0
#define ITEM_TYPE_BANK 1
//#define ITEM_TYPE_STOCK 2
//#define ITEM_TYPE_CARD 3

@implementation MainItemsController
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;

#pragma mark -
#pragma mark My Function

- (IBAction) doAddButtonClick {

	MainItemsAddController *mainItemsAddController = [[MainItemsAddController alloc] init];
	mainItemsAddController.title = NSLocalizedString(@"Добавить", @"Add Items");
	mainItemsAddController.m_db = self.m_db;
	mainItemsAddController.m_parent = self;
	[self.navigationController pushViewController:mainItemsAddController animated:YES];
	[mainItemsAddController release];
	
}

- (IBAction ) doEditButtonClick {
	
	[self.m_itemsTableView setEditing:!self.m_itemsTableView.editing animated:YES];
	
	if(self.m_itemsTableView.editing)
		[self.navigationItem.leftBarButtonItem setTitle:NSLocalizedString(@"Завершить", @"Done")];
	else 
		 [self.navigationItem.leftBarButtonItem setTitle:NSLocalizedString(@"Редакт.", @"Edit")];
}

- (void) doReloadData {

	self.m_itemsList = [self.m_db doSelectItemsTable];
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_itemsList retainCount], [self.m_itemsList count]);
	[self.m_itemsTableView reloadData];
}

#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {
	
	UIBarButtonItem *editButton = [[UIBarButtonItem alloc]
								   initWithTitle:NSLocalizedString(@"Редакт.", @"Edit")
								   style:UIBarButtonItemStyleBordered
								   target:self 
								   action:@selector(doEditButtonClick)];
	self.navigationItem.leftBarButtonItem = editButton;
	[editButton release];
	
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
								  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
								  target:self 
								  action:@selector(doAddButtonClick)];
	self.navigationItem.rightBarButtonItem = addButton;
	[addButton release];
	
	
	self.m_db = [[MoneySaverDatabase alloc] init];
	self.title = NSLocalizedString(@"MoneySaver", @"Program name");
    
	BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[self.m_db m_dbPath]];
	[m_db doOpenDatabase];
	[m_db doCreateItemsTable];
	[m_db doCreateBankbookTable];
	[m_db doCreateCategoryTable];
	[m_db doCreateAccountbookTable];
	
	if(fileExists == NO){
		[m_db doDeleteAllCategoryTable];
		
		NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
		NSString *image = @"icon-city.png";
		NSString *name = NSLocalizedString(@"Развлечения",@"cultural life");
		NSNumber *pid = [[NSNumber alloc] initWithInt:0];
		NSNumber *click = [[NSNumber alloc] initWithInt:0];
		NSNumber *income_type = [[NSNumber alloc] initWithInt:0];
		NSNumber *type = [[NSNumber alloc] initWithInt:1];
		
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[datas setObject:click forKey:@"click"];
		[datas setObject:type forKey:@"type"];
		
		[m_db doInsertCategoryTable:datas];
		
		//1
		image = @"icon-games.png";
		name = NSLocalizedString(@"Игры", @"Games");
        pid = [NSNumber numberWithInt:1];
        [datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
        
        
		//2
		image = @"icon-food.png";
		name = NSLocalizedString(@"Еда", @"Food");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
        //3
        image = @"icon-beer.png";
		name = NSLocalizedString(@"Напитки", @"Drink");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//4
		image = @"icon-movie.png";
		name = NSLocalizedString(@"Кино", @"Movie");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//5
		image = @"icon-sport.png";
		name = NSLocalizedString(@"Спорт", @"Sport");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//6
		image = @"icon-books.png";
		name = NSLocalizedString(@"Книги", @"Book");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//7
		image = @"icon-music.png";
		name = NSLocalizedString(@"Музыка", @"Music");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//8
		image = @"icon-houseexpense.png";
		name = NSLocalizedString(@"Домашние траты", @"HouseExpense");
		pid = [NSNumber numberWithInt:0];
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//9
		image = @"icon-phone.png";
		name = NSLocalizedString(@"Телефон", @"Phone");
		pid = [NSNumber numberWithInt:9];      //9
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//10
		image = @"icon-internet.png";
		name = NSLocalizedString(@"Интернет", @"Internet");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//11
		image = @"icon-cloths.png";
		name = NSLocalizedString(@"Одежда", @"Cloths");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//12
		image = @"icon-tex.png";
		name = NSLocalizedString(@"ЖКХ", @"Tex");
		pid = [NSNumber numberWithInt:0];
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//13
		image = @"icon-fire.jpeg";
		name = NSLocalizedString(@"Газ", @"Gas");
		pid = [NSNumber numberWithInt:13];    //13
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//14
		image = @"icon-water.jpeg";
		name = NSLocalizedString(@"Вода", @"Water");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//15
		image = @"icon-electronic.png";
		name = NSLocalizedString(@"Электричество", @"Electronic");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
		
		//16
        image = @"icon-car.png";
		name = NSLocalizedString(@"Автомобиль", @"car");
		pid = [NSNumber numberWithInt:0];
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[m_db doInsertCategoryTable:datas];
        //
        
        //17
		image = @"icon-parttimejob.png";
		name = NSLocalizedString(@"Подработка", @"Parttimejob");
		pid = [NSNumber numberWithInt:0];
		[datas setObject:pid forKey:@"pid"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[datas setObject:income_type forKey:@"type"];
		[m_db doInsertCategoryTable:datas];
		
		//18
		image = @"icon-sallary.png";
		name = NSLocalizedString(@"Зарплата", @"Sallary");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[datas setObject:income_type forKey:@"type"];
		[m_db doInsertCategoryTable:datas];
		
        //19
		image = @"icon-folder.png";
		name = NSLocalizedString(@"Выигрыш", @"Win");
		[datas setObject:name forKey:@"name"];
		[datas setObject:image forKey:@"image"];
		[datas setObject:income_type forKey:@"type"];
		[m_db doInsertCategoryTable:datas];
		
		[datas release];
		
		NSLog(@"DB FILE NOT EXISTS:%@", [self.m_db m_dbPath]);
	}else {
		NSLog(@"DB FILE EXISTS:%@", [self.m_db m_dbPath]);
		
	}
	[self doReloadData];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	[super viewDidUnload];
}

- (void)dealloc {
	[m_db doCloseDatabase];
	[m_db release];
	[m_itemsList release];
	[m_itemsTableView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MainTableIdentifier = @"MainTableIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MainTableIdentifier];
	
	if(cell == nil) {
		cell = [[[UITableViewCell alloc] 
				 initWithStyle:UITableViewCellStyleSubtitle
				 reuseIdentifier:MainTableIdentifier]
				autorelease];
	}
	
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	cell.textLabel.text = [data objectForKey:@"name"];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	NSNumber *type = [data objectForKey:@"type"];
	
	UIImage *image = nil;
	
	if([type intValue] == ITEM_TYPE_ACCOUNT) {
		cell.detailTextLabel.text = NSLocalizedString(@"Аккаунт", @"Accountbook");
		image = [UIImage imageNamed:@"icon-accountbook.png"];
		cell.imageView.image = image;
	}else if([type intValue] == ITEM_TYPE_BANK) {
		cell.detailTextLabel.text = NSLocalizedString(@"Банковские карты", @"Bankbook");
		image = [UIImage imageNamed:@"icon-bankbook.png"];
		cell.imageView.image = image;
	}
	return cell;
}

#pragma mark -
#pragma mark Table Delegate

- (void)tableView:(UITableView *)tableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	int type = [[data objectForKey:@"type"] intValue];
	
	switch (type) {
		case ITEM_TYPE_ACCOUNT:
			NSLog(@"ITEM_TYPE_ACCOUNT");
			AccountBookController *accountController = [[AccountBookController alloc] init];
			accountController.title = NSLocalizedString(@"Аккаунт", @"Accountbook");
			accountController.m_db = self.m_db;
			accountController.m_item_id = [[data objectForKey:@"id"] intValue];
			[self.navigationController pushViewController:accountController animated:YES];
			[accountController release];
			break;
		case ITEM_TYPE_BANK:
			NSLog(@"ITEM_TYPE_BANK");
			BankBookController *bankController = [[BankBookController alloc] init];
			bankController.title = NSLocalizedString(@"Банк. карты", @"Bankbook");
			bankController.m_db = self.m_db;
			bankController.m_item_id = [[data objectForKey:@"id"] intValue];
			[self.navigationController pushViewController:bankController animated:YES];
			[bankController release];
			break;
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSUInteger row = [indexPath row];
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	NSInteger item_id = [[data objectForKey:@"id"] intValue];
	if(![self.m_db doDeleteItemsTable:item_id]) {
		NSLog(@"ERROR DELETE BANKBOOK ITEM:%d", item_id);
		return;
	}
	
	[self.m_itemsList removeObjectAtIndex:row];
	[self.m_itemsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

@end

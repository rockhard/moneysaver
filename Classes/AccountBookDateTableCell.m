//  AccountBookDateTableCell.m

#import "AccountBookDateTableCell.h"

@implementation AccountBookDateTableCell
@synthesize m_dateLabel;
@synthesize m_incomeLabel;
@synthesize m_expenseLabel;
@synthesize m_countLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

}


- (void)dealloc {
	[m_dateLabel release];
	[m_incomeLabel release];
	[m_expenseLabel release];
	[m_countLabel release];
    [super dealloc];
}


@end

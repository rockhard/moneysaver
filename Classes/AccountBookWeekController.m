//  AccountBookWeekController.m


#import "AccountBookWeekController.h"
#import "AccountBookDateTableCell.h"
#import "MoneySaverDatabase.h"

@implementation AccountBookWeekController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;


- (void)viewDidLoad {
	self.m_itemsList = [self.m_db doSelectAccountbookTableWeekSum:m_item_id limit:60];
	
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_itemsList retainCount], [self.m_itemsList count]);	
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	
	[super viewDidUnload];
}


- (void)dealloc {
	[m_db release];
	[m_itemsList release];
	[m_itemsTableView release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DateCellIdentifier = @"DateCellIdentifier";
	
	AccountBookDateTableCell *cell = (AccountBookDateTableCell *)[tableView dequeueReusableCellWithIdentifier:DateCellIdentifier];
	
	if(cell == nil) {
				
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountBookDateTableCell" owner:self options:nil];
		
		for (id oneObject in nib)
			if([oneObject isKindOfClass:[AccountBookDateTableCell class]])
				cell = (AccountBookDateTableCell *)oneObject;
	}
	
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];

	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	cell.m_dateLabel.text = [data objectForKey:@"date"];
	cell.m_incomeLabel.text = [numberFormatter stringFromNumber:[data objectForKey:@"income_sum"]];
	cell.m_expenseLabel.text = [numberFormatter stringFromNumber:[data objectForKey:@"expense_sum"]];
    [numberFormatter release];
		
	return cell;
}

#pragma mark -
#pragma mark Table Delegate
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return NSLocalizedString(@"Дата             Доход      Расход", @"input Field");
}

@end

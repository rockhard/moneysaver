//  AccountBookTableCell.m

#import "AccountBookTableCell.h"

@implementation AccountBookTableCell
@synthesize m_imageView;
@synthesize m_categoryLabel;
@synthesize m_nameLabel;
@synthesize m_amountLabel, m_currency;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

}


- (void)dealloc {
	NSLog(@"tablecell");
	[m_imageView release];
	[m_categoryLabel release];
	[m_nameLabel release];
	[m_amountLabel release];
    [super dealloc];
}


@end

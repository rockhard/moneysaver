//  AccountBookAddDateController.m

#import "AccountBookAddDateController.h"
#import "AccountBookAddController.h"


@implementation AccountBookAddDateController
@synthesize m_datePicker;
@synthesize m_parent;
@synthesize m_date;

- (IBAction) doDoneButtonClick:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
	m_parent.m_date = [m_datePicker date];
	[m_parent.m_itemsTableView reloadData];
}


- (void)viewDidLoad {
	UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                   initWithTitle:NSLocalizedString(@"Готово", @"Done")	
                                   style: UIBarButtonItemStylePlain
                                   target:self 
								   action:@selector(doDoneButtonClick:)];
	self.navigationItem.rightBarButtonItem = doneButton;
	[doneButton release];
	
	[m_datePicker setDate:m_date];
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
}


- (void)dealloc {
    [super dealloc];
}


@end

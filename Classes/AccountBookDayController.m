//  AccountBookDayController.m


#import "AccountBookDayController.h"
#import "MoneySaverDatabase.h"
#import "AccountBookDateTableCell.h"
#import "AccountBookDayItemController.h"


@implementation AccountBookDayController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;

#pragma mark -
#pragma mark Override Function


- (void)viewDidLoad {
	self.m_itemsList = [self.m_db doSelectAccountbookTableDateSum:m_item_id limit:60];
	
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_itemsList retainCount], [self.m_itemsList count]);
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	
	[super viewDidUnload];
}


- (void)dealloc {
	[m_db release];
	[m_itemsList release];
	[m_itemsTableView release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DateCellIdentifier = @"DateCellIdentifier";
	
	AccountBookDateTableCell *cell = (AccountBookDateTableCell *)[tableView dequeueReusableCellWithIdentifier:DateCellIdentifier];
	
	if(cell == nil) {
		
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountBookDateTableCell" owner:self options:nil];
		
		for (id oneObject in nib)
			if([oneObject isKindOfClass:[AccountBookDateTableCell class]])
				cell = (AccountBookDateTableCell *)oneObject;
	}
	
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	
	cell.m_dateLabel.text = [data objectForKey:@"date"];
	
	cell.m_incomeLabel.text = [numberFormatter stringFromNumber:[data objectForKey:@"income_sum"]];
	cell.m_expenseLabel.text = [numberFormatter stringFromNumber:[data objectForKey:@"expense_sum"]];
	cell.m_countLabel.text = [[data objectForKey:@"count"] stringValue];
	
	[numberFormatter release];
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	return cell;
}

#pragma mark -
#pragma mark Table Delegate

///
-(void)viewWillAppear:(BOOL)animated {
    NSIndexPath *selectedIndexPath = [self.m_itemsTableView indexPathForSelectedRow];
    [self.m_itemsTableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    
    [self.m_itemsTableView reloadData];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return NSLocalizedString(@"Дата      Доходы    Расходы  кол", @"input Field");
} 


- (void)tableView:(UITableView *)tableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	AccountBookDayItemController *accountBookDayItemController = [[AccountBookDayItemController alloc] init];
	accountBookDayItemController.title = @"Общее за день";
	accountBookDayItemController.m_item_id = self.m_item_id;
	accountBookDayItemController.m_dateString = [data objectForKey:@"date"];
	accountBookDayItemController.m_db = self.m_db;
	
	[self.navigationController pushViewController:accountBookDayItemController animated:YES];
	[accountBookDayItemController release];
}

@end

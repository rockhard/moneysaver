//  MoneySaverAppDelegate.m


#import "MoneySaverAppDelegate.h"
#import "MoneySaverDatabase.h"

@implementation MoneySaverAppDelegate
@synthesize window;
@synthesize navigationController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    

	[window addSubview:navigationController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [window release];
	[navigationController release];
    [super dealloc];
}


@end

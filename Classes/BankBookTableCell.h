//  BankBookTableCell.h

#import <UIKit/UIKit.h>


@interface BankBookTableCell : UITableViewCell {	
	UIImageView *m_image;
	UILabel *m_nameLabel;
	UILabel *m_bankLabel;
	UILabel *m_accountsLabel;
}

@property (retain, nonatomic) IBOutlet UIImageView *m_image;
@property (retain, nonatomic) IBOutlet UILabel *m_nameLabel;
@property (retain, nonatomic) IBOutlet UILabel *m_bankLabel;
@property (retain, nonatomic) IBOutlet UILabel *m_accountsLabel;

@end

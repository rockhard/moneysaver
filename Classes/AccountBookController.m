//  AccountBookController.m

#import "AccountBookController.h"
#import "MoneySaverDatabase.h"
#import "AccountBookAddController.h"
#import "AccountBookCategoryController.h"
#import "AccountBookDayController.h"
#import "AccountBookWeekController.h"
#import "AccountBookMonthController.h"
#import "AccountBookTableCell.h"
#import "AccountBookDetailController.h"
#import "AccountBookStatisticController.h"

#define SECTION_COUNT			2
#define INPUT_FIELD_SECTION		0
#define ACCOUNT_LIST_SECTION	1

#define INPUT_FIELD_ROW			0
#define TITLE_FIELD_ROW			1
#define CATEGORY_FIELD_ROW		2


#define DAY_SEGEMENT_INDEX		0
#define WEEK_SEGEMENT_INDEX		1
#define MONTH_SEGEMENT_INDEX	2

#define INCOME_TYPE				0
#define EXPENSE_TYPE			1

@implementation AccountBookController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;

@synthesize m_mostClickCategoryDictionary;
@synthesize m_amountTextField;
///
@synthesize m_nameTextField;
@synthesize m_sumTextField;

#pragma mark -
#pragma mark My Function
- (IBAction) doKeyboardDoneButtonClick:(id)sender {
	if([self.m_amountTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Внимание!",@"title of Alert") 
														message:NSLocalizedString(@"Вы не ввели количество", @"You didn't input amount")
													   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", "OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
	}else {
		int pid = [[self.m_mostClickCategoryDictionary objectForKey:@"pid"] intValue];
		
		NSNumber *items_id = [[NSNumber alloc] initWithInt:m_item_id];
		NSNumber *category_pid = nil;
		if (pid == 0) {
			category_pid = [self.m_mostClickCategoryDictionary objectForKey:@"id"];
		}else {
			category_pid = [self.m_mostClickCategoryDictionary objectForKey:@"pid"];
		}

		NSNumber *category_id = [self.m_mostClickCategoryDictionary objectForKey:@"id"];
		NSNumber *bankbook_id = [[NSNumber alloc] initWithInt:-1];
		int category_type = [[self.m_mostClickCategoryDictionary objectForKey:@"type"] intValue];
		double amount_value = [self.m_amountTextField.text doubleValue];
		double income_value = 0;
		double expense_value = 0;
		if(category_type == EXPENSE_TYPE) 
			expense_value = amount_value;
		else
			income_value = amount_value;
		NSNumber *income = [[NSNumber alloc] initWithDouble:income_value];
		NSNumber *expense = [[NSNumber alloc] initWithDouble:expense_value];
		///
        NSNumber *amount = [[NSNumber alloc] initWithDouble:amount_value];
		NSString *name = @"";
        if(m_nameTextField.text != nil) name = m_nameTextField.text;
		NSString *description = @"";
		NSNumber *datetime = [[NSNumber alloc] initWithDouble:[[NSDate date] timeIntervalSince1970]];
		
		NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
		
		[datas setObject:items_id forKey:@"items_id"];
		[datas setObject:category_pid forKey:@"category_pid"];
		[datas setObject:category_id forKey:@"category_id"];
		[datas setObject:bankbook_id forKey:@"bankbook_id"];
	///
        [datas setObject:amount forKey:@"amount"];
		[datas setObject:income forKey:@"income"];
		[datas setObject:expense forKey:@"expense"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:description forKey:@"description"];
		[datas setObject:datetime forKey:@"datetime"];
		
		[items_id release];
		[income release];
		[expense release];
		//[name release];
		[description release];
		[datetime release];
		
		[m_db doInsertAccountbookTable:datas];
		[datas release];
		
		self.m_amountTextField.text = @"";
        self.m_nameTextField.text = @"";

		[self doReloadData];
	}
	
	[sender resignFirstResponder];	
}

- (IBAction) doAddButtonClick {
	AccountBookAddController *accountbookAddController = [[AccountBookAddController alloc] init];
	accountbookAddController.m_db = self.m_db;
	accountbookAddController.m_item_id = self.m_item_id;
	accountbookAddController.m_parent = self;
	
	[self.navigationController pushViewController:accountbookAddController animated:YES];
	[accountbookAddController release];
}

- (IBAction ) doEditButtonClick {
	
	[self.m_itemsTableView setEditing:!self.m_itemsTableView.editing animated:YES];
	
	if(self.m_itemsTableView.editing)
		[self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Завершить", @"Done")];
	else 
		[self.navigationItem.rightBarButtonItem setTitle:NSLocalizedString(@"Редакт.", @"Edit")];
}

- (void) doReloadData {
	self.m_itemsList = [m_db doSelectAccountbookTable:m_item_id number:30];
		
	[self.m_itemsTableView reloadData];
	
	NSMutableDictionary *sum_result = [m_db doSelectAccountbookTableSum:m_item_id];
	double income = [[sum_result objectForKey:@"income"] doubleValue];
	double expense = [[sum_result objectForKey:@"expense"] doubleValue];
	double result = income - expense;
	NSString *sumString = [[NSString alloc] initWithFormat:@"%.1f р.", result];
	self.m_sumTextField.text = sumString;
	[sumString release];

}

- (IBAction) doStatisticButtonClick:(id)sender {
	AccountBookStatisticController *accountbookStatisticController = [[AccountBookStatisticController alloc] init];
	accountbookStatisticController.m_db = self.m_db;
	accountbookStatisticController.m_item_id = self.m_item_id;
	
	[self.navigationController pushViewController:accountbookStatisticController animated:YES];
	[accountbookStatisticController release];
	
}

- (IBAction) doDateSegmentedButtonClick:(id)sender {
	UISegmentedControl *seg = (UISegmentedControl *)sender;
	NSLog(@"segemntButtonClick:%d", [seg selectedSegmentIndex]);
	if ([sender selectedSegmentIndex] == DAY_SEGEMENT_INDEX) {
		AccountBookDayController *accountbookDayController = [[AccountBookDayController alloc] init];
		accountbookDayController.title = NSLocalizedString(@"Дневной отчет", @"Daily Sum");
		accountbookDayController.m_item_id = self.m_item_id;
		accountbookDayController.m_db = self.m_db;
		[self.navigationController pushViewController:accountbookDayController animated:YES];
		[accountbookDayController release];
	}else if([sender selectedSegmentIndex] == WEEK_SEGEMENT_INDEX) {
		AccountBookWeekController *accountbookWeekController = [[AccountBookWeekController alloc] init];
		accountbookWeekController.title = NSLocalizedString(@"Недельный отчет", @"Weekily Sum");
		accountbookWeekController.m_item_id = self.m_item_id;
		accountbookWeekController.m_db = self.m_db;
		[self.navigationController pushViewController:accountbookWeekController animated:YES];
		[accountbookWeekController release];
	}else if([sender selectedSegmentIndex] == MONTH_SEGEMENT_INDEX) {
		AccountBookMonthController *accountbookMonthController = [[AccountBookMonthController alloc] init];
		accountbookMonthController.title = NSLocalizedString(@"Месячный отчет", @"Monthily Sum");
		accountbookMonthController.m_item_id = self.m_item_id;
		accountbookMonthController.m_db = self.m_db;
		[self.navigationController pushViewController:accountbookMonthController animated:YES];
		[accountbookMonthController release];
	}
	

	seg.selectedSegmentIndex = UISegmentedControlNoSegment;

}

#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {

	UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
								  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
								  target:self 
								  action:@selector(doAddButtonClick)];
	self.navigationItem.rightBarButtonItem = addButton;
	[addButton release];
	
	NSMutableArray *categoryResult = [m_db doSelectCategoryTable:1];
	
	
	if([categoryResult count] == 1) {
		self.m_mostClickCategoryDictionary = [categoryResult objectAtIndex:0];
	}
    
    self.m_sumTextField.layer.cornerRadius = 10;
	
	[self doReloadData];
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	
	self.m_mostClickCategoryDictionary = nil;
	self.m_amountTextField = nil;
    ///
    self.m_nameTextField = nil;

	self.m_sumTextField = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	[m_db release];
	[m_itemsList release];
	[m_itemsTableView release];
	[m_mostClickCategoryDictionary release];
	[m_amountTextField release];
    ///
    [m_nameTextField release];
	[m_sumTextField release];
	
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return SECTION_COUNT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	if(section == INPUT_FIELD_SECTION)
		///
        return 3;
	else
		return [self.m_itemsList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if(section == INPUT_FIELD_SECTION)
		return NSLocalizedString(@"Быстрый ввод", @"input Field");
	else
		return NSLocalizedString(@"Доходы/Расходы", @"input list");
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DataCellIdentifier = @"DataCellIdentifier";
	static NSString *InputCellIdentifier = @"InputCellIdentifier";
	
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];

	AccountBookTableCell *cell = nil;
	if(section == INPUT_FIELD_SECTION){
		cell = (AccountBookTableCell *)[tableView dequeueReusableCellWithIdentifier:InputCellIdentifier];
	
	}else{
		cell = (AccountBookTableCell *)[tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
	}
	
	if(cell == nil) {
		
		if(section == INPUT_FIELD_SECTION){
			cell = [[[UITableViewCell alloc]
					 initWithStyle:UITableViewCellStyleSubtitle
					reuseIdentifier:InputCellIdentifier] autorelease];
			
			if(row == INPUT_FIELD_ROW){
				UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
				textField.clearsOnBeginEditing = NO;
				//textField.textAlignment = UITextAlignmentRight;
				textField.placeholder = NSLocalizedString(@"Введите количество", @"Input amount");
				textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
				[textField setDelegate:self];
				textField.returnKeyType = UIReturnKeyDone;
				[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
					forControlEvents:UIControlEventEditingDidEndOnExit];
				[cell.contentView addSubview:textField];
				self.m_amountTextField = textField;
				[textField release];
			}
            
            if(row == TITLE_FIELD_ROW){
				UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
				textField.clearsOnBeginEditing = NO;
				//textField.textAlignment = UITextAlignmentRight;
				textField.placeholder = NSLocalizedString(@"Введите название, если нужно   ", @"Input name");
				textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
				[textField setDelegate:self];
				textField.returnKeyType = UIReturnKeyDone;
				[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
					forControlEvents:UIControlEventEditingDidEndOnExit];
				[cell.contentView addSubview:textField];
				self.m_nameTextField = textField;
				[textField release];
                
            }
            
            
		}else{
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountBookTableCell" owner:self options:nil];
			
			for (id oneObject in nib)
				if([oneObject isKindOfClass:[AccountBookTableCell class]])
					cell = (AccountBookTableCell *)oneObject;

		}
        //
       /* if(section == INPUT_FIELD_SECTION){
			cell = [[[UITableViewCell alloc]
					 initWithStyle:UITableViewCellStyleSubtitle
                     reuseIdentifier:InputCellIdentifier] autorelease];
			
			if(row == TITLE_FIELD_ROW){
				UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 300, 25)];
				textField.clearsOnBeginEditing = NO;
				//textField.textAlignment = UITextAlignmentRight;
				textField.placeholder = NSLocalizedString(@"Введите название", @"Input name");
				textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
				[textField setDelegate:self];
				textField.returnKeyType = UIReturnKeyDone;
				[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
					forControlEvents:UIControlEventEditingDidEndOnExit];
				[cell.contentView addSubview:textField];
				self.m_nameTextField = textField;
				[textField release];
                
            }
        
		}else{
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountBookTableCell" owner:self options:nil];
			
			for (id oneObject in nib)
				if([oneObject isKindOfClass:[AccountBookTableCell class]])
					cell = (AccountBookTableCell *)oneObject;
            
		}*/
        //
	}
	
	if(section == INPUT_FIELD_SECTION) {
		if(row == CATEGORY_FIELD_ROW) {
			if([self.m_mostClickCategoryDictionary count] != 0) {
				
				cell.textLabel.text = [self.m_mostClickCategoryDictionary objectForKey:@"name"];
				cell.detailTextLabel.text = NSLocalizedString(@"Категория", @"category");
				cell.imageView.image = [UIImage imageNamed:[self.m_mostClickCategoryDictionary objectForKey:@"image"]];
				
			}else {
				cell.textLabel.text = @"Не выбрано";
				cell.detailTextLabel.text = @"Category";
			}
		}else {
			
		}

	}else {
		NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
				int category_type = [[data objectForKey:@"type"] intValue];

		NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
		[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
		[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

		NSString *amountValue = nil;
		if(category_type == INCOME_TYPE) {
			amountValue = [numberFormatter stringFromNumber:[data objectForKey:@"income"]];
			cell.m_amountLabel.textColor = [UIColor orangeColor];
			
		}
		else {
			amountValue = [numberFormatter stringFromNumber:[data objectForKey:@"expense"]];
			cell.m_amountLabel.textColor = [UIColor blueColor];
			
		}
		cell.m_amountLabel.text = amountValue;
		NSLog(@"%@", amountValue);
		[numberFormatter release];
		
		cell.m_imageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
		cell.m_categoryLabel.text = [data objectForKey:@"image_name"];
		NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [data objectForKey:@"name"], [data objectForKey:@"date"]];
		cell.m_nameLabel.text = name;

	}
	
	return cell;
}

#pragma mark -
#pragma mark Table Delegate

///
-(void)viewWillAppear:(BOOL)animated {
    NSIndexPath *selectedIndexPath = [self.m_itemsTableView indexPathForSelectedRow];
    [self.m_itemsTableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    
    [self.m_itemsTableView reloadData];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	
	if((section == 0 && row == 1)||(section == 0 && row == 1))
		return nil;
	return indexPath;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	///
	if(section == 0 && row == 1){
		AccountBookAddController *accountbookAddController = [[AccountBookAddController alloc] init];
		accountbookAddController.m_db = self.m_db;
		accountbookAddController.m_item_id = self.m_item_id;
		accountbookAddController.m_parent = self;
		
		[self.navigationController pushViewController:accountbookAddController animated:YES];
		[accountbookAddController release];
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	if(section == INPUT_FIELD_SECTION){
		AccountBookCategoryController *accountBookCategoryController = [[AccountBookCategoryController alloc] init];
		accountBookCategoryController.title = NSLocalizedString(@"Категория", @"category");
		accountBookCategoryController.m_db = self.m_db;
		accountBookCategoryController.m_parent = self;
		
		[self.navigationController pushViewController:accountBookCategoryController animated:YES];
		[accountBookCategoryController release];
	}else{
		AccountBookDetailController *accountbookDetailController = [[AccountBookDetailController alloc] init];
		accountbookDetailController.title = NSLocalizedString(@"Изменить", @"Modify");
		accountbookDetailController.m_db = self.m_db;
		accountbookDetailController.m_parent = self;
		
		NSDictionary *data = [self.m_itemsList objectAtIndex:row];
		accountbookDetailController.m_id = [[data objectForKey:@"id"] intValue];
		
		[self.navigationController pushViewController:accountbookDetailController animated:YES];
		[accountbookDetailController release];
    
		
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	
	if(section == INPUT_FIELD_SECTION) {
		return;
	}else {
		NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
		NSInteger item_id = [[data objectForKey:@"id"] intValue];
		if(![self.m_db doDeleteAccountbookTable:item_id]) {
			NSLog(@"ERROR DELETE ACCOUNTBOOK ITEM:%d", item_id);
			return;
		}
		
		[self.m_itemsList removeObjectAtIndex:row];
		[self.m_itemsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

        self.m_amountTextField.text = @"";
		[self doReloadData];
	}
}
@end

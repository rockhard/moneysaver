//  AccountBookAddBankbookController.m


#import "AccountBookAddBankbookController.h"
#import "MoneySaverDatabase.h"
#import "BankBookTableCell.h"
#import "AccountBookAddController.h"


@implementation AccountBookAddBankbookController
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;
@synthesize m_parent;

#pragma mark -
#pragma mark My Function

- (void) doReloadData {
	self.m_itemsList = [self.m_db doSelectBankbookTable:-1];
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_itemsList retainCount], [self.m_itemsList count]);
	[self.m_itemsTableView reloadData];
}

#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {
	[self doReloadData];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	
	[self.m_db release];
	[self.m_itemsList release];
	[self.m_itemsTableView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CustomCellIdentifier = @"CustomCellIdentifier";
	
	BankBookTableCell *cell = (BankBookTableCell *)[tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
	if (cell == nil) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BankBookTableCell" owner:self options:nil];
		
		for (id oneObject in nib) 
			if([oneObject isKindOfClass:[BankBookTableCell class]])
				cell = (BankBookTableCell *)oneObject;
	}
	
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	cell.m_accountsLabel.text = [data objectForKey:@"account"];
	cell.m_bankLabel.text = [data objectForKey:@"bank"];
	cell.m_nameLabel.text = [data objectForKey:@"name"];
    cell.m_image.image = [UIImage imageNamed:@"icon-card.png"];

	return cell;
}

#pragma mark -
#pragma mark Table Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 77;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	[self.navigationController popViewControllerAnimated:YES];
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	m_parent.m_bankbookDictionary = data;
	[m_parent.m_itemsTableView reloadData];
}

@end

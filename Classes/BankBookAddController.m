//  BankBookAddController.m


#import "BankBookAddController.h"
#import "BankBookController.h"
#import "MoneySaverDatabase.h"

#define NUMBER_OF_DATA_ROWS	3
#define ACCOUNT_DATA_ROW_INDEX 0
#define BANK_DATA_ROW_INDEX 1
#define NAME_DATA_ROW_INDEX 2

@implementation BankBookAddController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;
@synthesize m_parent;

@synthesize m_accountTextField;
@synthesize m_bankTextField;
@synthesize m_nameTextField;

#pragma mark -
#pragma mark My Function
- (IBAction) doSaveButtonClick:(id)sender {
	if([self.m_nameTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Внимание!", @"title of Alert") 
														message:NSLocalizedString(@"Введите название!", @"Input Name") 
													   delegate:nil cancelButtonTitle:NSLocalizedString(@"OК", @"Alert OK") 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}else if([self.m_accountTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Внимание!", @"title of Alert") 
														message:NSLocalizedString(@"Введите аккаунт!", @"Input Account") 
													   delegate:nil cancelButtonTitle:NSLocalizedString(@"OК", @"Alert OK") 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}else if([self.m_bankTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Внимание!", @"title of Alert") 
														message:NSLocalizedString(@"Введите банк", @"Input bank") 
													   delegate:nil cancelButtonTitle:NSLocalizedString(@"OК", @"Alert OK") 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}else {
		NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
		
		NSNumber *dataItemsId = [[NSNumber alloc] initWithInt:self.m_item_id];
		NSNumber *dataDateTime = [[NSNumber alloc] initWithInt:[[NSDate date] timeIntervalSince1970]];
		
		[datas setObject:self.m_accountTextField.text forKey:@"account"];
		[datas setObject:self.m_bankTextField.text forKey:@"bank"];
		[datas setObject:self.m_nameTextField.text forKey:@"name"];
		[datas setObject:dataItemsId forKey:@"items_id"];
		[datas setObject:dataDateTime forKey:@"datetime"];
		
		[self.m_db doInsertBankbookTable:datas];
		[dataItemsId release];
		[dataDateTime release];
		[datas release];
		[self.navigationController popViewControllerAnimated:YES];
		[self.m_parent doReloadData];
		//[self.m_itemsTableView reloadData];
		
	}
}

- (IBAction) doKeyboardDoneButtonClick:(id)sender {
	[sender resignFirstResponder];	
}


#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"Сохранить", @"Save") 
                                                                   style: UIBarButtonItemStylePlain 
                                                                  target:self 
                                                                  action:@selector(doSaveButtonClick:)];
    
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
	
	NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:@"Введите номер", @"Введите банк", @"Введите имя", nil];
	self.m_itemsList = array;
	[array release];
    [super viewDidLoad];
}



- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	self.m_parent = nil;
	self.m_accountTextField = nil;
	self.m_bankTextField = nil;
	self.m_nameTextField = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	[m_db release];
	[m_itemsList release];
	[m_itemsTableView release];
	[m_parent release];
	[m_accountTextField release];
	[m_bankTextField release];
	[m_nameTextField release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return NUMBER_OF_DATA_ROWS;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MainTableIdentifier = @"BankBookTableIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MainTableIdentifier];
	
	NSUInteger row = [indexPath row];
	
	if(cell == nil) {
		cell = [[[UITableViewCell alloc] 
				 initWithStyle:UITableViewCellStyleSubtitle
				 reuseIdentifier:MainTableIdentifier]
				autorelease];
		
		UITextField *textField = [[UITextField alloc]
								  initWithFrame:CGRectMake(10, 10, 200, 25)];
		
		textField.clearButtonMode = NO;
		textField.placeholder = [self.m_itemsList objectAtIndex:row];
		[textField setDelegate:self];
		textField.returnKeyType = UIReturnKeyDone;
		[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
			forControlEvents:UIControlEventEditingDidEndOnExit];
		[cell.contentView addSubview:textField];
		if(row == NAME_DATA_ROW_INDEX) {
			self.m_nameTextField = textField;
		}else if(row == ACCOUNT_DATA_ROW_INDEX) {
			self.m_accountTextField = textField;
		}else if(row == BANK_DATA_ROW_INDEX) {
			self.m_bankTextField = textField;
		}
		[textField release];
		
		
	}
	

	return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return nil;
}

@end

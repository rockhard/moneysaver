//  BankBookTableCell.m


#import "BankBookTableCell.h"


@implementation BankBookTableCell
@synthesize m_image;
@synthesize m_nameLabel;
@synthesize m_bankLabel;
@synthesize m_accountsLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

}


- (void)dealloc {
	[m_image release];
	[m_nameLabel release];
	[m_bankLabel release];
	[m_accountsLabel release];
    [super dealloc];
}


@end

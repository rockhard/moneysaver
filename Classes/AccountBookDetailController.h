//  AccountBookDetailController.h


#import <UIKit/UIKit.h>

@class MoneySaverDatabase;
@class AccountBookController;

@interface AccountBookDetailController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	NSInteger				m_id;
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_itemsList;
	UITableView				*m_itemsTableView;
	
	AccountBookController	*m_parent;
	UITextField				*m_amountTextField;
	UITextField				*m_nameTextField;
	NSDate					*m_date;
	NSMutableDictionary		*m_bankbookDictionary;
	NSMutableDictionary		*m_categoryDictionary;
    UIBarButtonItem *saveButton;
}

@property (nonatomic)		  NSInteger				m_id;
@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) NSMutableArray		*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView	*m_itemsTableView;

@property (retain, nonatomic) AccountBookController *m_parent;
@property (retain, nonatomic) UITextField			*m_amountTextField;
@property (retain, nonatomic) UITextField			*m_nameTextField;
@property (retain, nonatomic) NSDate				*m_date;
@property (retain, nonatomic) NSMutableDictionary	*m_bankbookDictionary;
@property (retain, nonatomic) NSMutableDictionary	*m_categoryDictionary;
@property (retain, nonatomic) UIBarButtonItem *saveButton;


- (IBAction) doSaveButtonClick:(id)sender;
- (IBAction) doDeleteButtonClick:(id)sender;
- (IBAction) doKeyboardDoneButtonClick:(id)sender;
- (void) textFieldTouched:(id)sender;
@end

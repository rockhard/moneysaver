//  AccountBookCategoryController.m


#import "AccountBookCategoryController.h"
#import "AccountBookController.h"
#import "MoneySaverDatabase.h"


@implementation AccountBookCategoryController
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;
@synthesize m_parent;

#pragma mark -
#pragma mark My Function

- (void) doReloadData {
	self.m_itemsList = [self.m_db doSelectCategoryTable:0];

	[self.m_itemsTableView reloadData];
}

#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {
	[self doReloadData];
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	self.m_parent = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	[self.m_db release];
	[self.m_itemsList release];
	[self.m_itemsTableView release];
	[self.m_parent release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *MainTableIdentifier = @"MainTableIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MainTableIdentifier];
	
	if(cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MainTableIdentifier]
				autorelease];
	}
	
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	
	cell.textLabel.text = [data objectForKey:@"name"];	
	UIImage *image = [UIImage imageNamed:[data objectForKey:@"image"]];
	cell.imageView.image = image;
	
	return cell;
}

#pragma mark -
#pragma mark Table Delegate

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	
	int pid = [[data objectForKey:@"pid"] intValue];
	if(pid == 0)
		return 0;
	else
		return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	[self.navigationController popViewControllerAnimated:YES];
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	m_parent.m_mostClickCategoryDictionary = data;
	[m_parent.m_itemsTableView reloadData];
}

@end

//  BankBookAddController.h


#import <UIKit/UIKit.h>
@class MoneySaverDatabase;
@class BankBookController;

@interface BankBookAddController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	NSInteger				m_item_id;
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_itemsList;
	UITableView				*m_itemsTableView;
	BankBookController		*m_parent;
	
	UITextField				*m_accountTextField;
	UITextField				*m_bankTextField;
	UITextField				*m_nameTextField;
}

@property (nonatomic)		  NSInteger				m_item_id;
@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) NSMutableArray		*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView	*m_itemsTableView;
@property (retain, nonatomic) BankBookController	*m_parent;

@property (retain, nonatomic) UITextField *m_accountTextField;
@property (retain, nonatomic) UITextField *m_bankTextField;
@property (retain, nonatomic) UITextField *m_nameTextField;

- (IBAction) doSaveButtonClick:(id)sender;
- (IBAction) doKeyboardDoneButtonClick:(id)sender;

@end

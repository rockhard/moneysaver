//  MainItemsAddController.m


#import "MainItemsAddController.h"
#import "MainItemsController.h"
#import "MoneySaverDatabase.h"

#define NUMBER_OF_DATA_ROWS	2
#define NAME_DATA_ROW_INDEX 0
#define TYPE_DATA_ROW_INDEX 1

@implementation MainItemsAddController
@synthesize m_db;
@synthesize m_nameTextField;
@synthesize m_parent;
@synthesize m_typeSegmentedControl;

#pragma mark -
#pragma mark My Function

- (IBAction) doSaveButtonClick:(id)sender {
	if([self.m_nameTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Внимание!", @"title of Alert") 
													message:NSLocalizedString(@"Введите название", @"Input Name") 
													delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Alert OK") 
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}else {
		NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
		[datas setObject:self.m_nameTextField.text forKey:@"name"];
		
		NSNumber *type = [[NSNumber alloc] initWithInt:self.m_typeSegmentedControl.selectedSegmentIndex];
		[datas setObject:type forKey:@"type"];
		[type release];
		
		if(![self.m_db doInsertItemsTable:datas]) {
			[datas release];
			NSLog(@"insert Failed");
			return;
		}
		
		[datas release];
		[self.navigationController popViewControllerAnimated:YES];
		[self.m_parent doReloadData];
		
	}
}

- (IBAction) doKeyboardDoneButtonClick:(id)sender {
	[sender resignFirstResponder];	
}

#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"Сохранить", @"Save") 
                                                                   style: UIBarButtonItemStylePlain 
                                                                  target:self 
                                                                  action:@selector(doSaveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
	
	
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
    
	self.m_db = nil;
	self.m_parent = nil;
	self.m_nameTextField = nil;
	self.m_typeSegmentedControl = nil;
}

- (void)dealloc {
	
	[m_db release];
	[m_parent release];
	[m_nameTextField release];
	[m_typeSegmentedControl release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return NUMBER_OF_DATA_ROWS;
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DataCellIdentifier = @"DataCellIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
	
	NSUInteger row = [indexPath row];
	
	if(cell == nil) {
		
		cell = [[[UITableViewCell alloc]
				 initWithStyle:UITableViewCellStyleDefault 
				 reuseIdentifier:DataCellIdentifier] autorelease];
		
		switch (row) {
			case NAME_DATA_ROW_INDEX:
				NSLog(@"YOU MAKE WRONG CHOICE");
				UITextField *textField = [[UITextField alloc] 
										  initWithFrame:CGRectMake(10, 10, 200, 25)];
				textField.clearsOnBeginEditing = NO;
				textField.placeholder = NSLocalizedString(@"Введите название", @"Input Name");
				[textField setDelegate:self];
				textField.returnKeyType = UIReturnKeyDone;
				[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
					forControlEvents:UIControlEventEditingDidEndOnExit];
				[cell.contentView addSubview:textField];
				self.m_nameTextField = textField;
				[textField release];
				break;
			case TYPE_DATA_ROW_INDEX:
				NSLog(@"YOU MAKE WRONG CHOICE");
				NSArray *itemArray = [[NSArray alloc] initWithObjects:@"Аккаунт", @"Банковские карты", nil];
				UISegmentedControl *typeData = [[UISegmentedControl alloc]
												initWithItems:itemArray];
				typeData.selectedSegmentIndex = 0;
				typeData.frame = CGRectMake(10, 10, 280, 40);
				typeData.segmentedControlStyle = UISegmentedControlStyleBar;
				[itemArray release];
				[cell.contentView addSubview:typeData];
				self.m_typeSegmentedControl = typeData;
				[typeData release];
				break;
				
			default:
				NSLog(@"YOU MAKE WRONG CHOICE");
				break;
		}
	}
	
	return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	switch (row) {
		case TYPE_DATA_ROW_INDEX:
			return 60;
		default:
			return 50;
	}
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return nil;
}

@end

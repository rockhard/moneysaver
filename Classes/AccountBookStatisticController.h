//  AccountBookStatisticController.h

#import <UIKit/UIKit.h>

@class MoneySaverDatabase;

@interface AccountBookStatisticController : UIViewController
<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	NSInteger				m_item_id;
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_incomeItemsList;
	NSMutableArray			*m_expenseItemsList;
	UITableView				*m_itemsTableView;
	
	double					m_incomeSum;
	double					m_expenseSum;
}
@property (nonatomic)			NSInteger				m_item_id;
@property (retain, nonatomic)	MoneySaverDatabase		*m_db;
@property (retain, nonatomic)	NSMutableArray			*m_incomeItemsList;
@property (retain, nonatomic)	NSMutableArray			*m_expenseItemsList;
@property (retain, nonatomic)	IBOutlet UITableView	*m_itemsTableView;

@end

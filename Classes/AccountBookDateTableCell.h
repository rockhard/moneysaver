//  AccountBookDateTableCell.h

#import <UIKit/UIKit.h>

@interface AccountBookDateTableCell : UITableViewCell {
	UILabel *m_dateLabel;
	UILabel *m_incomeLabel;
	UILabel *m_expenseLabel;
	UILabel *m_countLabel;
}

@property (retain, nonatomic) IBOutlet UILabel *m_dateLabel;
@property (retain, nonatomic) IBOutlet UILabel *m_incomeLabel;
@property (retain, nonatomic) IBOutlet UILabel *m_expenseLabel;
@property (retain, nonatomic) IBOutlet UILabel *m_countLabel;

@end

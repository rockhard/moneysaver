//  AccountBookMonthController.h


#import <UIKit/UIKit.h>
@class MoneySaverDatabase;

@interface AccountBookMonthController : UIViewController 
<UITableViewDelegate, UITableViewDataSource> {
	NSInteger				m_item_id;
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_itemsList;
	UITableView				*m_itemsTableView;
}

@property (nonatomic)		  NSInteger				m_item_id;
@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) NSMutableArray		*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView	*m_itemsTableView;

@end

//  AccountBookAddDateController.h


#import <UIKit/UIKit.h>
@class AccountBookAddController;

@interface AccountBookAddDateController : UIViewController {
	UIDatePicker *m_datePicker;
	NSDate		 *m_date;
	
	AccountBookAddController *m_parent;
}
@property(retain, nonatomic) IBOutlet UIDatePicker *m_datePicker;
@property(retain, nonatomic) NSDate *m_date;
@property(retain, nonatomic) AccountBookAddController *m_parent;

- (IBAction) doDoneButtonClick:(id)sender;

@end

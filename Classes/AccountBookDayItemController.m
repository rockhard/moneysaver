//  AccountBookDayItemController.m


#import "AccountBookDayItemController.h"
#import "AccountBookTableCell.h"
#import "MoneySaverDatabase.h"

#define INCOME_TYPE				0
#define EXPENSE_TYPE			1

@implementation AccountBookDayItemController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;
@synthesize m_dateString;


- (void)viewDidLoad {
	self.m_itemsList = [self.m_db doSelectAccountbookTableDayItems:m_item_id number:self.m_dateString];
	
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_itemsList retainCount], [self.m_itemsList count]);
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	[m_db release];
	[m_itemsList release];
	[m_itemsTableView release];
	
    [super dealloc];
}

///
-(void)viewWillAppear:(BOOL)animated {
    NSIndexPath *selectedIndexPath = [self.m_itemsTableView indexPathForSelectedRow];
    [self.m_itemsTableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    
    [self.m_itemsTableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DataCellIdentifier = @"DataCellIdentifier";
	
	NSUInteger row = [indexPath row];
	
	AccountBookTableCell *cell = (AccountBookTableCell *)[tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
	
	if(cell == nil) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountBookTableCell" owner:self options:nil];
		
		for (id oneObject in nib)
			if([oneObject isKindOfClass:[AccountBookTableCell class]])
				cell = (AccountBookTableCell *)oneObject;
	}
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	int category_type = [[data objectForKey:@"type"] intValue];
	
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	NSString *amountValue = nil;
	if(category_type == INCOME_TYPE) {
		amountValue = [numberFormatter stringFromNumber:[data objectForKey:@"income"]];
		cell.m_amountLabel.textColor = [UIColor orangeColor];
	}else {
		amountValue = [numberFormatter stringFromNumber:[data objectForKey:@"expense"]];
		cell.m_amountLabel.textColor = [UIColor blueColor];
	}
	
	cell.m_amountLabel.text = amountValue;
	NSLog(@"%@", amountValue);
	[numberFormatter release];
	
	cell.m_imageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
	cell.m_categoryLabel.text = [data objectForKey:@"image_name"];
	NSString *name = [[NSString alloc] initWithFormat:@"%@ %@", [data objectForKey:@"name"], [data objectForKey:@"date"]];
	cell.m_nameLabel.text = name;
	
	return cell;
}


@end

//  AccountBookController.h

#import <UIKit/UIKit.h>

@class MoneySaverDatabase;

@interface AccountBookController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
	NSInteger				m_item_id;
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_itemsList;
	UITableView				*m_itemsTableView;
	
	NSMutableDictionary		*m_mostClickCategoryDictionary;
	UITextField				*m_amountTextField;
    ///
    UITextField				*m_nameTextField;

	UITextField				*m_sumTextField;
}

@property (nonatomic)		  NSInteger				m_item_id;
@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) NSMutableArray		*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView	*m_itemsTableView;

@property (retain, nonatomic) NSMutableDictionary	*m_mostClickCategoryDictionary;
@property (retain, nonatomic) IBOutlet UITextField	*m_amountTextField;
///
@property (retain, nonatomic) IBOutlet UITextField				*m_nameTextField;

@property (retain, nonatomic) IBOutlet UITextField	*m_sumTextField;

- (IBAction) doKeyboardDoneButtonClick:(id)sender;
- (IBAction) doAddButtonClick;
- (IBAction) doEditButtonClick;
- (void) doReloadData;

- (IBAction) doDateSegmentedButtonClick:(id)sender;
- (IBAction) doStatisticButtonClick:(id)sender;

@end

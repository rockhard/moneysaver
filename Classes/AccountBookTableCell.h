//  AccountBookTableCell.h

#import <UIKit/UIKit.h>

@interface AccountBookTableCell : UITableViewCell {
	UIImageView *m_imageView;
	UILabel		*m_categoryLabel;
	UILabel		*m_nameLabel;
	UILabel		*m_amountLabel;
    UILabel     *m_currency;
}

@property (retain, nonatomic) IBOutlet UIImageView	*m_imageView;
@property (retain, nonatomic) IBOutlet UILabel		*m_categoryLabel;
@property (retain, nonatomic) IBOutlet UILabel		*m_nameLabel;
@property (retain, nonatomic) IBOutlet UILabel		*m_amountLabel;
@property (assign, nonatomic) IBOutlet UILabel		*m_currency;


@end

//  AccountBookAddCategoryController.h


#import <UIKit/UIKit.h>
@class MoneySaverDatabase;
@class AccountBookAddController;

@interface AccountBookAddCategoryController : UIViewController {
	MoneySaverDatabase			*m_db;
	NSMutableArray				*m_itemsList;
	UITableView					*m_itemsTableView;
	
	AccountBookAddController	*m_parent;
	
}
@property (retain, nonatomic) MoneySaverDatabase		*m_db;
@property (retain, nonatomic) NSMutableArray			*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView		*m_itemsTableView;
@property (retain, nonatomic) AccountBookAddController	*m_parent;

- (void) doReloadData;
@end

//  BankBookController.m


#import "BankBookController.h"
#import "BankBookAddController.h"
#import "MoneySaverDatabase.h"
#import "BankBookTableCell.h"

@implementation BankBookController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;
@synthesize m_editBarButtonItem;

#pragma mark -
#pragma mark My Function

- (IBAction) doAddButtonClick {
	
	BankBookAddController *bankbookAddController = [[BankBookAddController alloc] init];
	bankbookAddController.title = NSLocalizedString(@"Добавить визитницу", @"add Bankbook");
	bankbookAddController.m_db = self.m_db;
	bankbookAddController.m_parent = self;
	bankbookAddController.m_item_id = self.m_item_id;
	[self.navigationController pushViewController:bankbookAddController animated:YES];
	[bankbookAddController release];
}

- (IBAction ) doEditButtonClick {
	
	[self.m_itemsTableView setEditing:!self.m_itemsTableView.editing animated:YES];
	
	if(m_itemsTableView.editing) {
		[self.m_editBarButtonItem setTitle: NSLocalizedString(@"Завершить", @"Done")];
	}else{
		[self.m_editBarButtonItem setTitle: NSLocalizedString(@"Редакт.", @"Edit")];
	}
}

- (void) doReloadData {
	self.m_itemsList = [self.m_db doSelectBankbookTable:self.m_item_id];
	/*
	if([self.m_itemsList retainCount] > 1)
		[self.m_itemsList release];
	 */
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_itemsList retainCount], [self.m_itemsList count]);
	[self.m_itemsTableView reloadData];
}

#pragma mark -
#pragma mark Override Function


- (void)viewDidLoad {
	UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
								  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
								  target:self 
								  action:@selector(doAddButtonClick)];
	self.navigationItem.rightBarButtonItem = addButton;
	[addButton release];
	[self doReloadData];
	
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
    
	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	self.m_editBarButtonItem = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	
	[self.m_db release];
	[self.m_itemsList release];
	[self.m_itemsTableView release];
	[self.m_editBarButtonItem release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.m_itemsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CustomCellIdentifier = @"CustomCellIdentifier";
	
	BankBookTableCell *cell = (BankBookTableCell *)[tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
	if (cell == nil) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BankBookTableCell" owner:self options:nil];
		
		for (id oneObject in nib) 
			if([oneObject isKindOfClass:[BankBookTableCell class]])
				cell = (BankBookTableCell *)oneObject;
	}
	
	NSUInteger row = [indexPath row];
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	
	cell.m_accountsLabel.text = [data objectForKey:@"account"];
	cell.m_bankLabel.text = [data objectForKey:@"bank"];
	cell.m_nameLabel.text = [data objectForKey:@"name"];
	//cell.m_image.image = [UIImage imageNamed:[data objectForKey:@"image"]];
	cell.m_image.image = [UIImage imageNamed:@"icon-card.png"];
	return cell;
}

#pragma mark -
#pragma mark Table Delegate

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

	NSUInteger row = [indexPath row];
	
	NSMutableDictionary *data = [self.m_itemsList objectAtIndex:row];
	NSInteger item_id = [[data objectForKey:@"id"] intValue];
	if(![self.m_db doDeleteBankbookTable:item_id]) {
		NSLog(@"ERROR DELETE BANKBOOK ITEM:%d", item_id);
		return;
	}
	
	[self.m_itemsList removeObjectAtIndex:row];
	[self.m_itemsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 77;
}

@end

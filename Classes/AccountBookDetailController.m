//  AccountBookDetailController.m





#import "AccountBookDetailController.h"
#import "AccountBookAddDateController.h"
#import "AccountBookAddCategoryController.h"
#import "AccountBookAddBankbookController.h"
#import "MoneySaverDatabase.h"


@implementation AccountBookDetailController
@synthesize m_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;

@synthesize m_parent;
@synthesize m_amountTextField;
@synthesize m_nameTextField;
@synthesize m_date;
@synthesize m_bankbookDictionary;
@synthesize m_categoryDictionary;
@synthesize saveButton;

#define NUMBER_OF_DATA_ROWS 5

#define AMOUNT_INPUT_ROW	0
#define CATEGORY_ROW		2
#define TITLE_ROW			1
#define DATE_ROW			3
#define BANKBOOK_ROW		4


#define INCOME_TYPE			0
#define EXPENSE_TYPE		1

#pragma mark -
#pragma mark My Function
- (IBAction) doSaveButtonClick:(id)sender {
	if([self.m_amountTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert",@"title of Alert") 
														message:NSLocalizedString(@"You didn't input amount", @"You didn't input amount")
													   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", "OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
	}else {
		int pid = [[self.m_categoryDictionary objectForKey:@"pid"] intValue];
		NSDictionary *data = [self.m_itemsList objectAtIndex:0];
        NSNumber *items_id = [data objectForKey:@"items_id"];
		NSNumber *category_pid = nil;
		if (pid == 0) {
			category_pid = [self.m_categoryDictionary objectForKey:@"id"];
		}else {
			category_pid = [self.m_categoryDictionary objectForKey:@"pid"];
		}
		NSNumber *category_id = [self.m_categoryDictionary objectForKey:@"id"];
		NSNumber *bankbook_id = nil;
		if(self.m_bankbookDictionary == nil)
			bankbook_id = [[NSNumber alloc] initWithInt:-1];
		else
			bankbook_id = [self.m_bankbookDictionary objectForKey:@"id"];
		
		double amountValue = [self.m_amountTextField.text doubleValue];
		int category_type = [[self.m_categoryDictionary objectForKey:@"type"] intValue];
		double income_value = 0;
		double expense_value = 0;
		if(category_type == EXPENSE_TYPE) 
			expense_value = amountValue;
		else
			income_value = amountValue;
		NSNumber *income = [[NSNumber alloc] initWithDouble:income_value];
		NSNumber *expense = [[NSNumber alloc] initWithDouble:expense_value];
		
		NSString *name = @"";
		if(m_nameTextField.text != nil) name = m_nameTextField.text;
		NSString *description = @"";
		NSNumber *datetime = [[NSNumber alloc] initWithDouble:[m_date timeIntervalSince1970]];
		
		NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
		[datas setObject:items_id forKey:@"items_id"];
		[datas setObject:category_pid forKey:@"category_pid"];
		[datas setObject:category_id forKey:@"category_id"];
		[datas setObject:bankbook_id forKey:@"bankbook_id"];
		[datas setObject:income forKey:@"income"];
		[datas setObject:expense forKey:@"expense"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:description forKey:@"description"];
		[datas setObject:datetime forKey:@"datetime"];
		
		[m_db doUpdateAccountbookTable:datas number:self.m_id];
		[items_id release];
		[income release];
		[expense release];
//		[name release];
//		[description release];
		[datetime release];
		
		[datas release];
		
		[self.navigationController popViewControllerAnimated:YES];
		[self.m_parent doReloadData];
	}
}

- (IBAction) doDeleteButtonClick:(id)sender {
	if(![self.m_db doDeleteAccountbookTable:self.m_id]){
		NSLog(@"ERROR DELETE ACCOUNTBOOK ITEM:%d", self.m_id);
		return;
	}
	
	[self.navigationController popViewControllerAnimated:YES];
	[self.m_parent doReloadData];	
}

- (IBAction) doKeyboardDoneButtonClick:(id)sender {
	[sender resignFirstResponder];	
    [self.navigationItem.rightBarButtonItem setEnabled:YES]; 
     
}
- (void) textFieldTouched:(id)sender{
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
}

#pragma mark -
#pragma mark Override Function


- (void)viewDidLoad {
/*	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]
								   initWithBarButtonSystemItem:UIBarButtonSystemItemSave
								   target:self 
								   action:@selector(doSaveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
    [saveButton release];*/
	saveButton = [[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"Сохранить", @"Save") 
                                                   style: UIBarButtonItemStylePlain 
                                                  target:self 
                                                  action:@selector(doSaveButtonClick:)];
    //saveButton = [[U
								  // initWithBarButtonSystemItem:UIBarButtonSystemItemSave
								 //  target: self 
								 //  action:@selector(doSaveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
   //  [self.navigationItem.rightBarButtonItem setTitle: NSLocalizedString(@"Сохранить", @"Save")];//saveButton; 
    // setTitle: NSLocalizedString(@"Завершить", @"Done")]
    [saveButton release];
    [self.navigationItem.rightBarButtonItem setEnabled:YES]; 

	self.m_itemsList = [m_db doSelectAccountbookTable:self.m_id];
	
	/*
     if([self.m_itemsList retainCount] > 1)
     [self.m_itemsList release];
	 */
	
	if([self.m_itemsList count] == 1) {
		NSDictionary *data = [self.m_itemsList objectAtIndex:0];
		self.m_date = [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"datetime"] doubleValue]];
		
		NSMutableArray *categoryResult = [self.m_db doSelectCategoryTableId:[[data objectForKey:@"category_id"] intValue]];
		/*
         if([categoryResult retainCount] > 1)
         [categoryResult release];
		 */
		
		if([categoryResult count] == 1)
			self.m_categoryDictionary = [categoryResult objectAtIndex:0];
		//[categoryResult release];
		
		NSMutableArray *bankbookResult = [self.m_db doSelectBankbookTableId:[[data objectForKey:@"bankbook_id"] intValue]];
		/*
         if([bankbookResult retainCount] > 1)
         [bankbookResult release];
         */
		
		if([bankbookResult count] == 1)
			self.m_bankbookDictionary = [bankbookResult objectAtIndex:0];
	}
	
    

    [super viewDidLoad];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	self.m_parent = nil;
	self.m_amountTextField = nil;
	self.m_nameTextField = nil;
	self.m_date = nil;
	self.m_bankbookDictionary = nil;
	self.m_categoryDictionary = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	
	[self.m_db release];
	[self.m_itemsList release];
	[self.m_itemsTableView release];
	[self.m_parent release];
//[self.m_amountTextField release];
//[self.m_nameTextField release];
	[self.m_date release];
	[self.m_bankbookDictionary release];
	[self.m_categoryDictionary release];
	NSLog(@"dealloc!!");
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return NUMBER_OF_DATA_ROWS;
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DataCellIdentifier = @"DataCellIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
	
	NSUInteger row = [indexPath row];
	
	if(cell == nil) {
		
		cell = [[[UITableViewCell alloc]
				 initWithStyle:UITableViewCellStyleSubtitle
				 reuseIdentifier:DataCellIdentifier] autorelease];
		
		if(row == AMOUNT_INPUT_ROW) {
            
			UITextField *textField = [[UITextField alloc] 
									  initWithFrame:CGRectMake(10, 10, 200, 25)];
			textField.clearsOnBeginEditing = NO;
			textField.placeholder = @"Введите количество";
			textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
			[textField setDelegate:self];
			textField.returnKeyType = UIReturnKeyDone;
			[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
				forControlEvents:UIControlEventEditingDidEndOnExit];
			[cell.contentView addSubview:textField];
            
            [textField addTarget:self action:@selector(textFieldTouched:) 
				forControlEvents:UIControlEventEditingDidBegin];
            
            
            
			self.m_amountTextField = textField;
		//	
			if([self.m_itemsList count] == 1) {
				NSDictionary *datas = [self.m_itemsList objectAtIndex:0];
				//NSString *test = [[data objectForKey:@"amount"] stringValue];
				float amount = 0.0;
				float income = [[datas objectForKey:@"income"] floatValue];
				float expense = [[datas objectForKey:@"expense"] floatValue];
				if(income > 0.0)
					amount = income;
				else
					amount = expense;
				
				NSNumber *amount_num = [[NSNumber alloc] initWithDouble:amount];
				textField.text = [amount_num stringValue];
				[amount_num release];
			}
			[textField release];
			
		}else if(row == TITLE_ROW) {
			UITextField *textField = [[UITextField alloc] 
									  initWithFrame:CGRectMake(10, 10, 200, 25)];
			textField.clearsOnBeginEditing = NO;
			textField.placeholder = @"Введите название";
			[textField setDelegate:self];
			textField.returnKeyType = UIReturnKeyDone;
			[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
				forControlEvents:UIControlEventEditingDidEndOnExit];
			[cell.contentView addSubview:textField];
            
            [textField addTarget:self action:@selector(textFieldTouched:) 
				forControlEvents:UIControlEventEditingDidBegin];

            
			self.m_nameTextField = textField;            
			if([self.m_itemsList count] == 1) {
				NSDictionary *datas = [self.m_itemsList objectAtIndex:0];
				//NSString *test= [data objectForKey:@"name"];
				textField.text = [datas objectForKey:@"name"];
               // [data release];
			}
			[textField release];

		}                

	}
	
	if(row == CATEGORY_ROW) {
		//cell.textLabel.text = @"Category(none)";
		if(self.m_categoryDictionary != nil){
			cell.textLabel.text = [self.m_categoryDictionary objectForKey:@"name"];
			cell.imageView.image = [UIImage imageNamed:[self.m_categoryDictionary objectForKey:@"image"]];
		}else{
			cell.textLabel.text = @"Не выбрано";
		}
		cell.detailTextLabel.text = NSLocalizedString(@"Категория", @"Category(none)");
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}else if(row == DATE_ROW) {
        [self.navigationItem.rightBarButtonItem setEnabled:YES]; 

        
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		cell.textLabel.text = [dateFormatter stringFromDate:self.m_date];
		cell.detailTextLabel.text = NSLocalizedString(@"Дата", @"date");
		cell.imageView.image = [UIImage imageNamed:@"icon-clock.png"];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		[dateFormatter release];
	}else if(row == BANKBOOK_ROW) {
                
		if(self.m_bankbookDictionary == nil){
			cell.textLabel.text = @"Не выбрано";
			cell.detailTextLabel.text = @"Банк. карта";
		}else{
			cell.textLabel.text = [self.m_bankbookDictionary objectForKey:@"account"];
			cell.detailTextLabel.text = [self.m_bankbookDictionary objectForKey:@"name"];
		}
		cell.imageView.image = [UIImage imageNamed:@"icon-card.png"];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
	
	return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	if(row == AMOUNT_INPUT_ROW || row == TITLE_ROW)
		return nil;
	else
		return indexPath;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	if(row == DATE_ROW) {
		AccountBookAddDateController *accountBookAddDateController = [[AccountBookAddDateController alloc] init];
		accountBookAddDateController.title = NSLocalizedString(@"Выберите дату", @"Select Date");
		accountBookAddDateController.m_parent = self;
		accountBookAddDateController.m_date = self.m_date;
		[self.navigationController pushViewController:accountBookAddDateController animated:YES];
		[accountBookAddDateController release];
	}else if(row == CATEGORY_ROW) {
		AccountBookAddCategoryController *accountBookAddCategoryController = [[AccountBookAddCategoryController alloc] init];
		accountBookAddCategoryController.title = NSLocalizedString(@"Выберите категорию", @"Select Category");
		accountBookAddCategoryController.m_db = self.m_db;
		accountBookAddCategoryController.m_parent = self;
		[self.navigationController pushViewController:accountBookAddCategoryController animated:YES];
		[accountBookAddCategoryController release];
		
	}else if(row == BANKBOOK_ROW) {
		AccountBookAddBankbookController *accountBookAddBankbookController = [[AccountBookAddBankbookController alloc] init];
		accountBookAddBankbookController.title = NSLocalizedString(@"Выберите банк. карту", @"Select Bankbook");
		accountBookAddBankbookController.m_db = self.m_db;
		accountBookAddBankbookController.m_parent = self;
		[self.navigationController pushViewController:accountBookAddBankbookController animated:YES];
		[accountBookAddBankbookController release];
	}
}

@end



/*
#import "AccountBookDetailController.h"
#import "AccountBookAddDateController.h"
#import "AccountBookAddCategoryController.h"
#import "AccountBookAddBankbookController.h"
#import "MoneySaverDatabase.h"


@implementation AccountBookDetailController
@synthesize m_id;
@synthesize m_db;
@synthesize m_itemsList;
@synthesize m_itemsTableView;

@synthesize m_parent;
@synthesize m_amountTextField;
@synthesize m_nameTextField;
@synthesize m_date;
@synthesize m_bankbookDictionary;
@synthesize m_categoryDictionary;

#define NUMBER_OF_DATA_ROWS 5

#define AMOUNT_INPUT_ROW	0
#define CATEGORY_ROW		2
#define TITLE_ROW			1
#define DATE_ROW			3
#define BANKBOOK_ROW		4


#define INCOME_TYPE			0
#define EXPENSE_TYPE		1

#pragma mark -
#pragma mark My Function
- (IBAction) doSaveButtonClick:(id)sender {
	if([self.m_amountTextField.text compare:@""] == NSOrderedSame) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Внимание!",@"title of Alert") 
														message:NSLocalizedString(@"Вы не ввели количество", @"You didn't input amount")
													   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", "OK") otherButtonTitles:nil];
		[alert show];
		[alert release];
	}else {
		int pid = [[self.m_categoryDictionary objectForKey:@"pid"] intValue];
		NSDictionary *data = [self.m_itemsList objectAtIndex:0];
		NSNumber *items_id = [data objectForKey:@"items_id"];
		NSNumber *category_pid = nil;
		if (pid == 0) {
			category_pid = [self.m_categoryDictionary objectForKey:@"id"];
		}else {
			category_pid = [self.m_categoryDictionary objectForKey:@"pid"];
		}
		NSNumber *category_id = [self.m_categoryDictionary objectForKey:@"id"];
		NSNumber *bankbook_id = nil;
		if(self.m_bankbookDictionary == nil)
			bankbook_id = [[NSNumber alloc] initWithInt:-1];
		else
			bankbook_id = [self.m_bankbookDictionary objectForKey:@"id"];
		
		double amountValue = [self.m_amountTextField.text doubleValue];
		int category_type = [[self.m_categoryDictionary objectForKey:@"type"] intValue];
		double income_value = 0;
		double expense_value = 0;
		if(category_type == EXPENSE_TYPE) 
			expense_value = amountValue;
		else
			income_value = amountValue;
		NSNumber *income = [[NSNumber alloc] initWithDouble:income_value];
		NSNumber *expense = [[NSNumber alloc] initWithDouble:expense_value];
		
		NSString *name = @"";
		if(m_nameTextField.text != nil) name = m_nameTextField.text;
		NSString *description = @"";
		NSNumber *datetime = [[NSNumber alloc] initWithDouble:[m_date timeIntervalSince1970]];
		
		NSMutableDictionary *datas = [[NSMutableDictionary alloc] init];
		
		[datas setObject:items_id forKey:@"items_id"];
		[datas setObject:category_pid forKey:@"category_pid"];
		[datas setObject:category_id forKey:@"category_id"];
		[datas setObject:bankbook_id forKey:@"bankbook_id"];
		[datas setObject:income forKey:@"income"];
		[datas setObject:expense forKey:@"expense"];
		[datas setObject:name forKey:@"name"];
		[datas setObject:description forKey:@"description"];
		[datas setObject:datetime forKey:@"datetime"];
		
		[m_db doUpdateAccountbookTable:datas number:self.m_id];
		
		[items_id release];
		[income release];
		[expense release];
		[name release];
		[description release];
		[datetime release];
		

		[datas release];
		
		[self.navigationController popViewControllerAnimated:YES];
		[self.m_parent doReloadData];
	}
}

- (IBAction) doDeleteButtonClick:(id)sender {
	if(![self.m_db doDeleteAccountbookTable:self.m_id]){
		NSLog(@"ERROR DELETE ACCOUNTBOOK ITEM:%d", self.m_id);
		return;
	}
	
	[self.navigationController popViewControllerAnimated:YES];
	[self.m_parent doReloadData];	
}

- (IBAction) doKeyboardDoneButtonClick:(id)sender {
	[sender resignFirstResponder];	
}

#pragma mark -
#pragma mark Override Function

- (void)viewDidLoad {
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]
								   initWithBarButtonSystemItem:UIBarButtonSystemItemSave
								   target:self 
								   action:@selector(doSaveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
	
	self.m_itemsList = [m_db doSelectAccountbookTable:self.m_id];
	
	if([self.m_itemsList count] == 1) {
		NSDictionary *data = [self.m_itemsList objectAtIndex:0];
		self.m_date = [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"datetime"] doubleValue]];
		
		NSMutableArray *categoryResult = [self.m_db doSelectCategoryTableId:[[data objectForKey:@"category_id"] intValue]];
		
		if([categoryResult count] == 1)
			self.m_categoryDictionary = [categoryResult objectAtIndex:0];
		
		NSMutableArray *bankbookResult = [self.m_db doSelectBankbookTableId:[[data objectForKey:@"bankbook_id"] intValue]];
				
		if([bankbookResult count] == 1)
			self.m_bankbookDictionary = [bankbookResult objectAtIndex:0];
	}
	
	
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {

	self.m_db = nil;
	self.m_itemsList = nil;
	self.m_itemsTableView = nil;
	self.m_parent = nil;
	self.m_amountTextField = nil;
	self.m_nameTextField = nil;
	self.m_date = nil;
	self.m_bankbookDictionary = nil;
	self.m_categoryDictionary = nil;
	[super viewDidUnload];
}


- (void)dealloc {
	
	[self.m_db release];
	[self.m_itemsList release];
	[self.m_itemsTableView release];
	[self.m_parent release];
	[self.m_date release];
	[self.m_bankbookDictionary release];
	[self.m_categoryDictionary release];
	NSLog(@"dealloc");
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return NUMBER_OF_DATA_ROWS;
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *DataCellIdentifier = @"DataCellIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DataCellIdentifier];
	
	NSUInteger row = [indexPath row];
	
	if(cell == nil) {
		
		cell = [[[UITableViewCell alloc]
				 initWithStyle:UITableViewCellStyleSubtitle
				 reuseIdentifier:DataCellIdentifier] autorelease];
		
		if(row == AMOUNT_INPUT_ROW) {
			UITextField *textField = [[UITextField alloc] 
									  initWithFrame:CGRectMake(10, 10, 200, 25)];
			textField.clearsOnBeginEditing = NO;
			textField.placeholder = @"Введите количество";
			textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
			[textField setDelegate:self];
			textField.returnKeyType = UIReturnKeyDone;
			[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
				forControlEvents:UIControlEventEditingDidEndOnExit];
			[cell.contentView addSubview:textField];
			self.m_amountTextField = textField;
			
			if([self.m_itemsList count] == 1) {
				NSDictionary *data = [self.m_itemsList objectAtIndex:0];
				float amount = 0.0;
				float income = [[data objectForKey:@"income"] floatValue];
				float expense = [[data objectForKey:@"expense"] floatValue];
				if(income > 0.0)
					amount = income;
				else
					amount = expense;
				
				NSNumber *amount_num = [[NSNumber alloc] initWithDouble:amount];
				textField.text = [amount_num stringValue];
				[amount_num release];
			}
			[textField release];
			
		}else if(row == TITLE_ROW) {
			UITextField *textField = [[UITextField alloc] 
									  initWithFrame:CGRectMake(10, 10, 200, 25)];
			textField.clearsOnBeginEditing = NO;
			textField.placeholder = @"Введите название";
			[textField setDelegate:self];
			textField.returnKeyType = UIReturnKeyDone;
			[textField addTarget:self action:@selector(doKeyboardDoneButtonClick:) 
				forControlEvents:UIControlEventEditingDidEndOnExit];
			[cell.contentView addSubview:textField];
			self.m_nameTextField = textField;
			if([self.m_itemsList count] == 1) {
				NSDictionary *data = [self.m_itemsList objectAtIndex:0];
				textField.text = [data objectForKey:@"name"];
                [data release];
			}
			[textField release];
			
		}
	}
	
	if(row == CATEGORY_ROW) {
		if(self.m_categoryDictionary != nil){
			cell.textLabel.text = [self.m_categoryDictionary objectForKey:@"name"];
			cell.imageView.image = [UIImage imageNamed:[self.m_categoryDictionary objectForKey:@"image"]];
		}else{
			cell.textLabel.text = @"None";
		}
		cell.detailTextLabel.text = NSLocalizedString(@"Категория", @"Category(none)");
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}else if(row == DATE_ROW) {
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
		[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
		cell.textLabel.text = [dateFormatter stringFromDate:self.m_date];
		cell.detailTextLabel.text = NSLocalizedString(@"Дата", @"date");
		cell.imageView.image = [UIImage imageNamed:@"icon-clock.png"];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		[dateFormatter release];
	}else if(row == BANKBOOK_ROW) {
		if(self.m_bankbookDictionary == nil){
			cell.textLabel.text = @"Не выбрано";
			cell.detailTextLabel.text = @"Визитница";
		}else{
			cell.textLabel.text = [self.m_bankbookDictionary objectForKey:@"account"];
			cell.detailTextLabel.text = [self.m_bankbookDictionary objectForKey:@"name"];
		}
		cell.imageView.image = [UIImage imageNamed:@"icon-folder.png"];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}
	
	return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	if(row == AMOUNT_INPUT_ROW || row == TITLE_ROW)
		return nil;
	else
		return indexPath;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = [indexPath row];
	
	if(row == DATE_ROW) {
		AccountBookAddDateController *accountBookAddDateController = [[AccountBookAddDateController alloc] init];
		accountBookAddDateController.title = NSLocalizedString(@"Выберите дату", @"Select Date");
		accountBookAddDateController.m_parent = self;
		accountBookAddDateController.m_date = self.m_date;
		[self.navigationController pushViewController:accountBookAddDateController animated:YES];
		[accountBookAddDateController release];
	}else if(row == CATEGORY_ROW) {
		AccountBookAddCategoryController *accountBookAddCategoryController = [[AccountBookAddCategoryController alloc] init];
		accountBookAddCategoryController.title = NSLocalizedString(@"Выберите категорию", @"Select Category");
		accountBookAddCategoryController.m_db = self.m_db;
		accountBookAddCategoryController.m_parent = self;
		[self.navigationController pushViewController:accountBookAddCategoryController animated:YES];
		[accountBookAddCategoryController release];
		
	}else if(row == BANKBOOK_ROW) {
		AccountBookAddBankbookController *accountBookAddBankbookController = [[AccountBookAddBankbookController alloc] init];
		accountBookAddBankbookController.title = NSLocalizedString(@"Выберите визитницу", @"Select Bankbook");
		accountBookAddBankbookController.m_db = self.m_db;
		accountBookAddBankbookController.m_parent = self;
		[self.navigationController pushViewController:accountBookAddBankbookController animated:YES];
		[accountBookAddBankbookController release];
	}
}

@end
 */

//  MainItemsController.h


#import <UIKit/UIKit.h>
@class MoneySaverDatabase;
@class MainItemsAddController;

@interface MainItemsController : UIViewController 
<UITableViewDelegate, UITableViewDataSource> {
	MoneySaverDatabase		*m_db;
	NSMutableArray			*m_itemsList;
	UITableView				*m_itemsTableView;
}

@property (retain, nonatomic) MoneySaverDatabase	*m_db;
@property (retain, nonatomic) NSMutableArray		*m_itemsList;
@property (retain, nonatomic) IBOutlet UITableView	*m_itemsTableView;

- (IBAction) doAddButtonClick;
- (IBAction) doEditButtonClick;
- (void) doReloadData;

@end

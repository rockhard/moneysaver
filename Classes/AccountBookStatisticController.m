//  AccountBookStatisticController.m

#import "AccountBookStatisticController.h"
#import "AccountBookTableCell.h"
#import "MoneySaverDatabase.h"
#import "AccountBookController.h"

#define SECTION_COUNT			2
#define INCOME_TYPE				0
#define EXPENSE_TYPE			1

@implementation AccountBookStatisticController
@synthesize m_item_id;
@synthesize m_db;
@synthesize m_incomeItemsList;
@synthesize m_expenseItemsList;
@synthesize m_itemsTableView;


#pragma mark -
#pragma mark My Function

- (void) doReloadData {
	self.m_incomeItemsList = [m_db doSelectAccountbookTableAllStatistic:m_item_id type:INCOME_TYPE];
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_incomeItemsList retainCount], [self.m_incomeItemsList count]);
	
	self.m_expenseItemsList = [m_db doSelectAccountbookTableAllStatistic:m_item_id type:EXPENSE_TYPE];
	NSLog(@"Retain Count:%d itmeListCount:%d", [self.m_expenseItemsList retainCount], [self.m_expenseItemsList count]);
	
	NSMutableDictionary *sum_result = [m_db doSelectAccountbookTableSum:m_item_id];
	m_incomeSum = [[sum_result objectForKey:@"income"] doubleValue];
	m_expenseSum = [[sum_result objectForKey:@"expense"] doubleValue];
	
	[self.m_itemsTableView reloadData];
}

#pragma mark -
#pragma mark Override Function


- (void)viewDidLoad {
	[self doReloadData];
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark -
#pragma mark Table Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return SECTION_COUNT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	if(section == 0)
		return [self.m_incomeItemsList count];
	else
		return [self.m_expenseItemsList count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if(section == 0)
		return NSLocalizedString(@"Доход", @"Income");
	else
		return NSLocalizedString(@"Расход", @"Expense");
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *IncomeCellIdentifier = @"IncomeCellIdentifier";
	static NSString *ExpenseCellIdentifier = @"ExpenseCellIdentifier";
	
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	
	AccountBookTableCell *cell = nil;
	
	if(section == INCOME_TYPE)
		(AccountBookTableCell *)[tableView dequeueReusableCellWithIdentifier:IncomeCellIdentifier];
	else
		(AccountBookTableCell *)[tableView dequeueReusableCellWithIdentifier:ExpenseCellIdentifier];

	if (cell == nil) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountBookTableCell" owner:self options:nil];
       

		for (id oneObject in nib)
			if ([oneObject isKindOfClass:[AccountBookTableCell class]])
				cell = (AccountBookTableCell *)oneObject;
	}	
	
    
	if(section == INCOME_TYPE){
		NSMutableDictionary *data = [self.m_incomeItemsList objectAtIndex:row];
		cell.m_imageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
		cell.m_categoryLabel.text = [data objectForKey:@"name"];
		
		NSString *amount = [[NSString alloc] initWithFormat:@"%@/%.0f", [data objectForKey:@"income"], m_incomeSum];
		cell.m_nameLabel.text = amount;
		[amount release];
		
		double percent = ([[data objectForKey:@"income"] doubleValue] / m_incomeSum) * 100;
		NSString *percent_str = [[NSString alloc] initWithFormat:@"%.1f%%", percent];
		cell.m_amountLabel.text = percent_str;
		cell.m_amountLabel.textColor = [UIColor orangeColor];
		[percent_str release];
	}else {
		NSMutableDictionary *data = [self.m_expenseItemsList objectAtIndex:row];
		cell.m_imageView.image = [UIImage imageNamed:[data objectForKey:@"image"]];
		cell.m_categoryLabel.text = [data objectForKey:@"name"];
		
		NSString *amount = [[NSString alloc] initWithFormat:@"%@/%.0f", [data objectForKey:@"expense"], m_expenseSum];
		cell.m_nameLabel.text = amount;
		[amount release];
		
		double percent = ([[data objectForKey:@"expense"] doubleValue] / m_expenseSum) * 100;
		NSString *percent_str = [[NSString alloc] initWithFormat:@"%.1f%%", percent];
		cell.m_amountLabel.text = percent_str;
		cell.m_amountLabel.textColor = [UIColor blueColor];
		[percent_str release];
	}

	//[cell.m_currency setHidden:YES];
    //NSNumber integ = [NSNumber numberWithFloat:0.0];
    cell.m_currency.alpha = 0.0;
	return cell;
}

@end
